//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
// Copyright(C)      2021 Andrew Apted
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef __ENGINE_API__
#define __ENGINE_API__

//
//  doomtype.h
// 

#if defined(_MSC_VER) && !defined(__cplusplus)
#define inline __inline
#endif


//
// The packed attribute forces structures to be packed into the minimum 
// space necessary.  If this is not done, the compiler may align structure
// fields differently to optimize memory access, inflating the overall
// structure size.  It is important to use the packed attribute on certain
// structures where alignment is important, particularly data read/written
// to disk.
//

#ifdef __GNUC__

#if defined(_WIN32) && !defined(__clang__)
#define PACKEDATTR __attribute__((packed,gcc_struct))
#else
#define PACKEDATTR __attribute__((packed))
#endif

#else
#define PACKEDATTR
#endif

#ifdef __WATCOMC__
#define PACKEDPREFIX _Packed
#else
#define PACKEDPREFIX
#endif

#define PACKED_STRUCT(...) PACKEDPREFIX struct __VA_ARGS__ PACKEDATTR

// C99 integer types; with gcc we just use this.  Other compilers
// should add conditional statements that define the C99 types.

// What is really wanted here is stdint.h; however, some old versions
// of Solaris don't have stdint.h and only have inttypes.h (the 
// pre-standardisation version).  inttypes.h is also in the C99 
// standard and defined to include stdint.h, so include this. 

#include <inttypes.h>

typedef uint8_t boolean;

enum 
{
    false, 
    true
};

typedef uint8_t byte;
typedef uint8_t pixel_t;

#include <limits.h>

#define arrlen(array) (sizeof(array) / sizeof(*array))


//
// DOOM keyboard definition.
//
// This is the stuff configured by Setup.Exe.
// Most key data are simple ascii (uppercased).
//
#define KEY_ESCAPE      27
#define KEY_ENTER       13
#define KEY_TAB         9

#define KEY_RIGHTARROW  (0x80 + 0x2e)
#define KEY_LEFTARROW   (0x80 + 0x2c)
#define KEY_UPARROW     (0x80 + 0x2d)
#define KEY_DOWNARROW   (0x80 + 0x2f)

#define KEY_F1          (0x80 + 0x3b)
#define KEY_F2          (0x80 + 0x3c)
#define KEY_F3          (0x80 + 0x3d)
#define KEY_F4          (0x80 + 0x3e)
#define KEY_F5          (0x80 + 0x3f)
#define KEY_F6          (0x80 + 0x40)
#define KEY_F7          (0x80 + 0x41)
#define KEY_F8          (0x80 + 0x42)
#define KEY_F9          (0x80 + 0x43)
#define KEY_F10         (0x80 + 0x44)
#define KEY_F11         (0x80 + 0x57)
#define KEY_F12         (0x80 + 0x58)

#define KEY_BACKSPACE   0x7f
#define KEY_PAUSE       0xff
#define KEY_EQUALS      0x3d
#define KEY_MINUS       0x2d

#define KEY_SHIFT      (0x80 + 0x36)
#define KEY_CTRL       (0x80 + 0x1d)
#define KEY_ALT        (0x80 + 0x38)

// new keys

#define KEY_CAPSLOCK    (0x80 + 0x3a)
#define KEY_NUMLOCK     (0x80 + 0x45)
#define KEY_SCRLCK      (0x80 + 0x46)
#define KEY_PRTSCR      (0x80 + 0x59)

#define KEY_HOME        (0x80 + 0x47)
#define KEY_END         (0x80 + 0x4f)
#define KEY_PGUP        (0x80 + 0x49)
#define KEY_PGDN        (0x80 + 0x51)
#define KEY_INS         (0x80 + 0x52)
#define KEY_DEL         (0x80 + 0x53)

// keypad : aliases of other keys

#define KEYP_0          KEY_INS
#define KEYP_1          KEY_END
#define KEYP_2          KEY_DOWNARROW
#define KEYP_3          KEY_PGDN
#define KEYP_4          KEY_LEFTARROW
#define KEYP_5          (0x80 + 0x4c)
#define KEYP_6          KEY_RIGHTARROW
#define KEYP_7          KEY_HOME
#define KEYP_8          KEY_UPARROW
#define KEYP_9          KEY_PGUP

#define KEYP_DIVIDE     '/'
#define KEYP_PLUS       '+'
#define KEYP_MINUS      '-'
#define KEYP_MULTIPLY   '*'
#define KEYP_PERIOD     KEY_DEL
#define KEYP_EQUALS     KEY_EQUALS
#define KEYP_ENTER      KEY_ENTER


//
// Event handling.
//

typedef enum
{
    // Key press/release events.
    //    data1: Key code (from doomkeys.h) of the key that was
    //           pressed or released. This is the key as it appears
    //           on a US keyboard layout, and does not change with
    //           layout.
    // For ev_keydown only:
    //    data2: ASCII representation of the key that was pressed that
    //           changes with the keyboard layout; eg. if 'Z' is
    //           pressed on a German keyboard, data1='y',data2='z'.
    //           Not affected by modifier keys.
    //    data3: ASCII input, fully modified according to keyboard
    //           layout and any modifier keys that are held down.
    //           Only set if I_StartTextInput() has been called.
    ev_keydown,
    ev_keyup,

    // Mouse movement event.
    //    data1: Bitfield of buttons currently held down.
    //           (bit 0 = left; bit 1 = right; bit 2 = middle).
    //    data2: X axis mouse movement (turn).
    //    data3: Y axis mouse movement (forward/backward).
    ev_mouse,

    // Joystick state.
    //    data1: Bitfield of buttons currently pressed.
    //    data2: X axis mouse movement (turn).
    //    data3: Y axis mouse movement (forward/backward).
    //    data4: Third axis mouse movement (strafe).
    //    data5: Fourth axis mouse movement (look)
    ev_joystick,

    // Quit event. Triggered when the user clicks the "close" button
    // to terminate the application.
    ev_quit
} evtype_t;

typedef struct
{
    evtype_t type;

    // event-specific data; see the descriptions given above.
    int data1, data2, data3, data4, data5;
} event_t;

// Called by IO functions when input is detected.
void D_PostEvent (event_t *ev);


//
// D_DoomMain()
//
// Not a globally visible function, just included for source reference,
// calls all startup code, parses command line options.
//

extern boolean singletics;

void D_DoomMain (void);

void D_ShutdownEngine (void);

boolean D_WantGrabMouse (void);
 

//
//  v_video
//

void V_RestoreBuffer(void);


#endif  /* __ENGINE_API__ */
