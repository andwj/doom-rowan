;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const GLOWSPEED    =  8
const STROBEBRIGHT =  5
const FASTDARK     = 15
const SLOWDARK     = 35

type fireflicker_t = struct {
	.thinker    thinker_t
	.sector    ^sector_t

	.count      s32
	.maxlight   s16
	.minlight   s16
}

type lightflash_t = struct {
	.thinker    thinker_t
	.sector    ^sector_t

	.count      s32
	.maxlight   s16
	.minlight   s16

	.maxtime    s32
	.mintime    s32
}

type strobe_t = struct {
	.thinker    thinker_t
	.sector    ^sector_t

	.count      s32
	.minlight   s16
	.maxlight   s16

	.darktime   s32
	.brighttime s32
}

type glow_t = struct {
	.thinker    thinker_t
	.sector    ^sector_t

	.minlight   s16
	.maxlight   s16
	.direction  s16
}

;
; P_SpawnFireFlicker
;
fun P_SpawnFireFlicker (sec ^sector_t) {
	let flick = cast ^fireflicker_t Z_Calloc fireflicker_t.size PU_LEVEL

	[flick .thinker .func] = THINK_flicker

	[flick .sector]   = sec
	[flick .maxlight] = [sec .light]
	[flick .minlight] = P_FindMinSurroundingLight sec [sec .light]
	[flick .minlight] = [flick .minlight] + 16
	[flick .count]    = 4

	P_AddThinker (cast ^thinker_t flick)

	; note that we are resetting sector attributes.
	; nothing else is special about it during gameplay.
	[sec .special] = 0
}

fun T_FireFlicker (flick ^fireflicker_t) {
	[flick .count] = [flick .count] - 1

	if [flick .count] == 0 {
		let r = P_Random
		r = (r & 3) * 16
		let amount = s16 r

		let sec = [flick .sector]

		if [sec .light] - amount < [flick .minlight] {
			[sec .light] = [flick .minlight]
		} else {
			[sec .light] = [flick .maxlight] - amount
		}

		[flick .count] = 4
	}
}

;
; P_SpawnLightFlash
; Do flashing lights.
;
fun P_SpawnLightFlash (sec ^sector_t) {
	let flash = cast ^lightflash_t Z_Calloc lightflash_t.size PU_LEVEL

	[flash .thinker .func] = THINK_flash

	[flash .sector]   = sec
	[flash .maxlight] = [sec .light]
	[flash .minlight] = P_FindMinSurroundingLight sec [sec .light]
	[flash .maxtime]  = 64
	[flash .mintime]  = 7

	let r = P_Random
	r = r & [flash .maxtime]
	[flash .count] = r + 1

	P_AddThinker (cast ^thinker_t flash)

	[sec .special] = 0
}

fun T_LightFlash (flash ^lightflash_t) {
	[flash .count] = [flash .count] - 1

	if [flash .count] == 0 {
		let sec = [flash .sector]
		let r   = P_Random

		if [sec .light] == [flash .maxlight] {
			r = r & [flash .mintime]
			[sec   .light] = [flash .minlight]
			[flash .count] = r + 1
		} else {
			r = r & [flash .maxtime]
			[sec   .light] = [flash .maxlight]
			[flash .count] = r + 1
		}
	}
}

;
; P_SpawnStrobeFlash
; We're gonna have a ball tonight...
;
fun P_SpawnStrobeFlash (sec ^sector_t, darktime s32, sync bool) {
	let flash = cast ^strobe_t Z_Calloc strobe_t.size PU_LEVEL

	[flash .thinker .func] = THINK_strobe

	[flash .sector]     = sec
	[flash .darktime]   = darktime
	[flash .brighttime] = STROBEBRIGHT
	[flash .maxlight]   = [sec .light]
	[flash .minlight]   = P_FindMinSurroundingLight sec [sec .light]

	if [flash .minlight] == [flash .maxlight] {
		[flash .minlight] = 0
	}

	if sync {
		[flash .count] = 1
	} else {
		let r = P_Random
		r = r & 7
		[flash .count] = r + 1
	}

	P_AddThinker (cast ^thinker_t flash)

	[sec .special] = 0
}

fun T_StrobeFlash (flash ^strobe_t) {
	[flash .count] = [flash .count] - 1

	if [flash .count] == 0 {
		let sec = [flash .sector]

		if [sec .light] == [flash .minlight] {
			[sec   .light] = [flash .maxlight]
			[flash .count] = [flash .brighttime]
		} else {
			[sec   .light] = [flash .minlight]
			[flash .count] = [flash .darktime]
		}
	}
}

;
; Spawn glowing light
;
fun P_SpawnGlowingLight (sec ^sector_t) {
	let glow = cast ^glow_t Z_Calloc glow_t.size PU_LEVEL

	[glow .thinker .func] = THINK_glow

	[glow .sector]    = sec
	[glow .maxlight]  = [sec .light]
	[glow .minlight]  = P_FindMinSurroundingLight sec [sec .light]
	[glow .direction] = -1

	P_AddThinker (cast ^thinker_t glow)

	[sec .special] = 0
}

fun T_Glow (glow ^glow_t) {
	let sec = [glow .sector]

	if [glow .direction] < 0 {
		; DOWN
		let light = [sec .light] - GLOWSPEED

		if light > [glow .minlight] {
			[sec .light] = light
		} else {
			[glow .direction] = +1
		}

	} else if [glow .direction] > 0 {
		; UP
		let light = [sec .light] + GLOWSPEED

		if light < [glow .maxlight] {
			[sec .light] = light
		} else {
			[glow .direction] = -1
		}
	}
}

;
; Start strobing lights (usually from a trigger)
;
fun EV_StartLightStrobing (ld ^line_t) {
	let secnum = s32 -1

	loop {
		secnum = P_FindSectorFromLineTag ld secnum
		break if secnum < 0

		let sec = [ref sectors secnum]

		if null? [sec .specialdata] {
			P_SpawnStrobeFlash sec SLOWDARK FALSE
		}
	}
}

;
; TURN LINE'S TAG LIGHTS OFF
;
fun EV_LightTurnOff (ld ^line_t) {
	let i = s32 0
	loop while i < numsectors {
		let sec = [ref sectors i]

		if [sec .tag] == [ld .tag] {
			[sec .light] = P_FindMinSurroundingLight sec [sec .light]
		}

		i = i + 1
	}
}

;
; TURN LINE'S TAG LIGHTS ON
; bright = 0 means to search for highest light level in surrounding sectors.
;
fun EV_LightTurnOn (ld ^line_t, bright s16) {
	let i = s32 0
	loop while i < numsectors {
		let sec = [ref sectors i]

		if [sec .tag] == [ld .tag] {
			if bright == 0 {
				bright = P_FindMaxSurroundingLight sec 0
			}

			[sec .light] = bright
		}

		i = i + 1
	}
}

;
; Find maximum light from an adjacent sector
;
fun P_FindMaxSurroundingLight (sec ^sector_t, low s16 -> s16) {
	let high = low

	let i = s32 0
	loop while i < [sec .linecount] {
		let ld    = [[sec .lines] i]
		let check = getNextSector ld sec

		if ref? check {
			high = max high [check .light]
		}

		i = i + 1
	}

	return low
}

;
; Find minimum light from an adjacent sector
;
fun P_FindMinSurroundingLight (sec ^sector_t, high s16 -> s16) {
	let low = high

	let i = s32 0
	loop while i < [sec .linecount] {
		let ld    = [[sec .lines] i]
		let check = getNextSector ld sec

		if ref? check {
			low = min low [check .light]
		}

		i = i + 1
	}

	return low
}
