;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

;
; Keyboard controls
;
var key_right s32 = KEY_RIGHTARROW
var key_left  s32 = KEY_LEFTARROW
var key_up    s32 = KEY_UPARROW
var key_down  s32 = KEY_DOWNARROW

var key_strafeleft  s32 = ','
var key_straferight s32 = '.'

var key_fire   s32 = KEY_CTRL
var key_use    s32 = ' '
var key_strafe s32 = KEY_ALT
var key_speed  s32 = KEY_SHIFT
var key_jump   s32 = '/'

;
; Mouse buttons
;
var mouseb_fire         s32 = 0
var mouseb_strafe       s32 = 1
var mouseb_forward      s32 = 2
var mouseb_backward     s32 = -1
var mouseb_jump         s32 = -1

var mouseb_strafeleft   s32 = -1
var mouseb_straferight  s32 = -1
var mouseb_use          s32 = -1

var mouseb_prevweapon   s32 = -1
var mouseb_nextweapon   s32 = -1

; miscellaneous keys:

var key_msg_refresh s32 = KEY_ENTER
var key_pause       s32 = KEY_PAUSE
var key_demo_quit   s32 = 'q'
var key_spy         s32 = KEY_F12

; weapon selection keys:

var key_weapon1    s32 = '1'
var key_weapon2    s32 = '2'
var key_weapon3    s32 = '3'
var key_weapon4    s32 = '4'
var key_weapon5    s32 = '5'
var key_weapon6    s32 = '6'
var key_weapon7    s32 = '7'
var key_weapon8    s32 = '8'
var key_prevweapon s32 = 0
var key_nextweapon s32 = 0

; automap keys:

var key_map_north    s32 = KEY_UPARROW
var key_map_south    s32 = KEY_DOWNARROW
var key_map_east     s32 = KEY_RIGHTARROW
var key_map_west     s32 = KEY_LEFTARROW
var key_map_zoomin   s32 = '='
var key_map_zoomout  s32 = '-'
var key_map_toggle   s32 = KEY_TAB
var key_map_maxzoom  s32 = '0'
var key_map_follow   s32 = 'f'
var key_map_grid     s32 = 'g'
var key_map_mark     s32 = 'm'
var key_map_clear    s32 = 'c'

; menu keys:

var key_menu_activate s32 = KEY_ESCAPE
var key_menu_up       s32 = KEY_UPARROW
var key_menu_down     s32 = KEY_DOWNARROW
var key_menu_left     s32 = KEY_LEFTARROW
var key_menu_right    s32 = KEY_RIGHTARROW
var key_menu_back     s32 = KEY_BACKSPACE
var key_menu_forward  s32 = KEY_ENTER
var key_menu_confirm  s32 = 'y'
var key_menu_abort    s32 = 'n'

var key_menu_help     s32 = KEY_F1
var key_menu_save     s32 = KEY_F2
var key_menu_load     s32 = KEY_F3
var key_menu_volume   s32 = KEY_F4
var key_menu_detail   s32 = KEY_F5
var key_menu_qsave    s32 = KEY_F6
var key_menu_endgame  s32 = KEY_F7
var key_menu_messages s32 = KEY_F8
var key_menu_qload    s32 = KEY_F9
var key_menu_quit     s32 = KEY_F10
var key_menu_gamma    s32 = KEY_F11

var key_menu_incscreen  s32 = KEY_EQUALS
var key_menu_decscreen  s32 = KEY_MINUS
var key_menu_screenshot s32 = 0

; Control whether if a mouse button is double clicked, it acts like
; "use" has been pressed

var dclick_use s32 = 1

;
; Bind all of the common controls used by Doom and all other games.
;
fun M_BindBaseControls () {
	M_BindIntVariable "key_right"            [ref key_right]
	M_BindIntVariable "key_left"             [ref key_left]
	M_BindIntVariable "key_up"               [ref key_up]
	M_BindIntVariable "key_down"             [ref key_down]
	M_BindIntVariable "key_strafeleft"       [ref key_strafeleft]
	M_BindIntVariable "key_straferight"      [ref key_straferight]
	M_BindIntVariable "key_fire"             [ref key_fire]
	M_BindIntVariable "key_use"              [ref key_use]
	M_BindIntVariable "key_strafe"           [ref key_strafe]
	M_BindIntVariable "key_speed"            [ref key_speed]

	M_BindIntVariable "mouseb_fire"          [ref mouseb_fire]
	M_BindIntVariable "mouseb_strafe"        [ref mouseb_strafe]
	M_BindIntVariable "mouseb_forward"       [ref mouseb_forward]

	; Extra controls that are not in the Vanilla versions:
	M_BindIntVariable "mouseb_strafeleft"    [ref mouseb_strafeleft]
	M_BindIntVariable "mouseb_straferight"   [ref mouseb_straferight]
	M_BindIntVariable "mouseb_use"           [ref mouseb_use]
	M_BindIntVariable "mouseb_backward"      [ref mouseb_backward]
	M_BindIntVariable "key_pause"            [ref key_pause]
	M_BindIntVariable "key_message_refresh"  [ref key_msg_refresh]

	; Config items
	M_BindIntVariable "dclick_use"           [ref dclick_use]
}

fun M_BindWeaponControls () {
	M_BindIntVariable "key_weapon1"          [ref key_weapon1]
	M_BindIntVariable "key_weapon2"          [ref key_weapon2]
	M_BindIntVariable "key_weapon3"          [ref key_weapon3]
	M_BindIntVariable "key_weapon4"          [ref key_weapon4]
	M_BindIntVariable "key_weapon5"          [ref key_weapon5]
	M_BindIntVariable "key_weapon6"          [ref key_weapon6]
	M_BindIntVariable "key_weapon7"          [ref key_weapon7]
	M_BindIntVariable "key_weapon8"          [ref key_weapon8]

	M_BindIntVariable "key_prevweapon"       [ref key_prevweapon]
	M_BindIntVariable "key_nextweapon"       [ref key_nextweapon]

	M_BindIntVariable "mouseb_prevweapon"    [ref mouseb_prevweapon]
	M_BindIntVariable "mouseb_nextweapon"    [ref mouseb_nextweapon]
}

fun M_BindMapControls () {
	M_BindIntVariable "key_map_north"        [ref key_map_north]
	M_BindIntVariable "key_map_south"        [ref key_map_south]
	M_BindIntVariable "key_map_east"         [ref key_map_east]
	M_BindIntVariable "key_map_west"         [ref key_map_west]
	M_BindIntVariable "key_map_zoomin"       [ref key_map_zoomin]
	M_BindIntVariable "key_map_zoomout"      [ref key_map_zoomout]
	M_BindIntVariable "key_map_toggle"       [ref key_map_toggle]
	M_BindIntVariable "key_map_maxzoom"      [ref key_map_maxzoom]
	M_BindIntVariable "key_map_follow"       [ref key_map_follow]
	M_BindIntVariable "key_map_grid"         [ref key_map_grid]
	M_BindIntVariable "key_map_mark"         [ref key_map_mark]
	M_BindIntVariable "key_map_clearmark"    [ref key_map_clear]
}

fun M_BindMenuControls () {
	M_BindIntVariable "key_menu_activate"    [ref key_menu_activate]
	M_BindIntVariable "key_menu_up"          [ref key_menu_up]
	M_BindIntVariable "key_menu_down"        [ref key_menu_down]
	M_BindIntVariable "key_menu_left"        [ref key_menu_left]
	M_BindIntVariable "key_menu_right"       [ref key_menu_right]
	M_BindIntVariable "key_menu_back"        [ref key_menu_back]
	M_BindIntVariable "key_menu_forward"     [ref key_menu_forward]
	M_BindIntVariable "key_menu_confirm"     [ref key_menu_confirm]
	M_BindIntVariable "key_menu_abort"       [ref key_menu_abort]

	M_BindIntVariable "key_menu_help"        [ref key_menu_help]
	M_BindIntVariable "key_menu_save"        [ref key_menu_save]
	M_BindIntVariable "key_menu_load"        [ref key_menu_load]
	M_BindIntVariable "key_menu_volume"      [ref key_menu_volume]
	M_BindIntVariable "key_menu_detail"      [ref key_menu_detail]
	M_BindIntVariable "key_menu_qsave"       [ref key_menu_qsave]
	M_BindIntVariable "key_menu_endgame"     [ref key_menu_endgame]
	M_BindIntVariable "key_menu_messages"    [ref key_menu_messages]
	M_BindIntVariable "key_menu_qload"       [ref key_menu_qload]
	M_BindIntVariable "key_menu_quit"        [ref key_menu_quit]
	M_BindIntVariable "key_menu_gamma"       [ref key_menu_gamma]

	M_BindIntVariable "key_menu_incscreen"   [ref key_menu_incscreen]
	M_BindIntVariable "key_menu_decscreen"   [ref key_menu_decscreen]
	M_BindIntVariable "key_menu_screenshot"  [ref key_menu_screenshot]
	M_BindIntVariable "key_demo_quit"        [ref key_demo_quit]
	M_BindIntVariable "key_spy"              [ref key_spy]
}
