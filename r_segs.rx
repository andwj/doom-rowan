;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const HEIGHTBITS = 12
const HEIGHTUNIT = 1 << HEIGHTBITS

const COLUMN_DRAWN = 0x7777

type drawseg_t = struct {
	.x1  s32  ; pixel range, inclusive
	.x2  s32  ;

	.seg  ^seg_t

	.topclip     ^[0]s16  ; clipping info for potential sprite blockers
	.bottomclip  ^[0]s16  ; [ used for masked walls too ]

	.texcolumns  ^[0]s16  ; saved tex column calcs for masked walls

	.solid       bool  ; completely solid, e.g. 1-sided wall
	.masked      bool  ; has a masked wall
	.sil_floor   bool  ; from floor downwards is solid, clips
	.sil_ceil    bool  ; from ceiling upwards is solid, clips

	.scale1      fixed_t
	.scale2      fixed_t
	.scalestep   fixed_t

	.floor_height  fixed_t  ; do not clip sprites above this
	.ceil_height   fixed_t  ; do not clip sprites below this
}

; andrewj: raised this from 256 to 1024
const MAXDRAWSEGS = 1024

zero-var drawsegs    [MAXDRAWSEGS]drawseg_t
zero-var numdrawseg  s32

; andrewj: doubled this limit
const MAXOPENINGS = SCREENWIDTH * 128

zero-var openings    [MAXOPENINGS]s16
zero-var numopening  s32

zero-var markfloor      bool
zero-var markceiling    bool

zero-var toptexture     s32
zero-var bottomtexture  s32
zero-var midtexture     s32

;
; regular wall
;
; WISH: encapsulate all these into a `rw` variable
zero-var rw_centerangle       angle_t
zero-var rw_offset            fixed_t
zero-var rw_distance          fixed_t
zero-var rw_scale             fixed_t
zero-var rw_scalestep         fixed_t
zero-var rw_midtexturemid     fixed_t
zero-var rw_toptexturemid     fixed_t
zero-var rw_bottomtexturemid  fixed_t

zero-var rw_normalangle  angle_t
zero-var rw_angle1       angle_t

zero-var topfrac      fixed_t
zero-var topstep      fixed_t
zero-var bottomfrac   fixed_t
zero-var bottomstep   fixed_t

zero-var pixhigh      fixed_t
zero-var pixlow       fixed_t
zero-var pixhighstep  fixed_t
zero-var pixlowstep   fixed_t

zero-var walllight s32

;
; R_ClearDrawSegs
;
fun R_ClearDrawSegs () {
	numdrawseg = 0
	numopening = 0
}

;
; R_StoreWallRange
;
; A wall segment will be drawn between start and stop pixels (inclusive).
;
fun R_StoreWallRange (x1 s32, x2 s32) {
	; don't overflow and crash
	if numdrawseg >= MAXDRAWSEGS {
		return
	}

	; allocate a drawseg
	let ds = [ref drawsegs numdrawseg]
	numdrawseg = numdrawseg + 1

	let seg = curline

	let sidedef = [seg .sidedef]
	let linedef = [seg .linedef]

	; mark the segment as visible for automap
	[linedef .flags] = [linedef .flags] | ML_MAPPED

	; calculate rw_distance for scale calculation
	rw_normalangle = [seg .angle] + ANG90

	let offsetangle = s32 (rw_normalangle - rw_angle1)
	offsetangle = abs offsetangle
	offsetangle = min offsetangle ANG90

	let distangle = u32 (ANG90 - offsetangle)
	distangle = distangle >> ANGLEFINESHIFT

	let hyp = R_PointToDist viewx viewy [[seg .v1] .x] [[seg .v1] .y]
	rw_distance = FixedMul hyp [finesine distangle]

	let numcolumn = x2 - x1 - -1

	[ds .x1]  = x1
	[ds .x2]  = x2
	[ds .seg] = seg

	; calculate scale at both ends and step
	let curangle = viewangle + [xtoviewangle x1]
	[ds .scale1] = R_ScaleFromGlobalAngle curangle

	if x2 > x1 {
		curangle = viewangle + [xtoviewangle x2]
		[ds .scale2] = R_ScaleFromGlobalAngle curangle

		let step = [ds .scale2] - [ds .scale1]
		step = step / (x2 - x1)

		[ds .scalestep] = step
	} else {
		[ds .scale2]    = [ds .scale1]
		[ds .scalestep] = 0
	}

	rw_scale     = [ds .scale1]
	rw_scalestep = [ds .scalestep]

	; calculate texture boundaries.
	; also decide if floor / ceiling marks are needed.
	let frontsector = [cursubsec .sector]
	let backsector  = [seg .back]

	let worldtop    = [frontsector .ceilh]  - viewz
	let worldbottom = [frontsector .floorh] - viewz
	let worldhigh   = s32 0
	let worldlow    = s32 0

	midtexture    = 0
	toptexture    = 0
	bottomtexture = 0

	[ds .solid]      = FALSE
	[ds .masked]     = FALSE
	[ds .sil_floor]  = FALSE
	[ds .sil_ceil]   = FALSE

	[ds .bottomclip] = NULL
	[ds .topclip]    = NULL
	[ds .texcolumns] = NULL

	; enough openings left?
	let openings_left = MAXOPENINGS - numopening
	openings_left = openings_left - numcolumn - numcolumn - numcolumn
	let no_openings   = openings_left < 0

	if null? backsector {
		; single sided line
		midtexture = [texture_translation [sidedef .mid]]

		; a single sided line is terminal, so it must mark ends
		markfloor   = TRUE
		markceiling = TRUE

		if ([linedef .flags] & ML_DONTPEGBOTTOM) != 0 {
			let vtop = R_TextureHeight [sidedef .mid]
			vtop = vtop + [frontsector .floorh]

			; bottom of texture at bottom
			rw_midtexturemid = vtop - viewz
		} else {
			; top of texture at top
			rw_midtexturemid = worldtop
		}

		rw_midtexturemid = rw_midtexturemid + [sidedef .y_offset]

		[ds .solid] = TRUE

		; force all sprites to be clipped
		[ds .floor_height] = FRAC_MAX
		[ds .ceil_height]  = FRAC_MIN
	} else {
		; two sided line
		worldhigh = [backsector .ceilh]  - viewz
		worldlow  = [backsector .floorh] - viewz

		; hack to allow height changes in outdoor areas
		if [frontsector .ceilpic] == skyflatnum {
			if [backsector .ceilpic] == skyflatnum {
				worldtop = worldhigh
			}
		}

		markfloor = TRUE

		if worldlow == worldbottom {
			if [backsector .floorpic] == [frontsector .floorpic] {
				if [backsector .light] == [frontsector .light] {
					markfloor = FALSE
				}
			}
		}

		markceiling = TRUE

		if worldhigh == worldtop {
			if [backsector .ceilpic] == [frontsector .ceilpic] {
				if [backsector .light] == [frontsector .light] {
					markceiling = FALSE
				}
			}
		}

		if [frontsector .floorh] > [backsector .floorh] {
			[ds .sil_floor]    = TRUE
			[ds .floor_height] = [frontsector .floorh]
		} else if [backsector .floorh] > viewz {
			[ds .sil_floor]    = TRUE
			[ds .floor_height] = FRAC_MAX
		}

		if [frontsector .ceilh] < [backsector .ceilh] {
			[ds .sil_ceil]    = TRUE
			[ds .ceil_height] = [frontsector .ceilh]
		} else if [backsector .ceilh] < viewz {
			[ds .sil_ceil]    = TRUE
			[ds .ceil_height] = FRAC_MIN
		}

		if [backsector .ceilh] <= [frontsector .floorh] {
			[ds .solid] = TRUE

			markfloor   = TRUE  ; closed door
			markceiling = TRUE
		}

		if [backsector .floorh] >= [frontsector .ceilh] {
			[ds .solid] = TRUE

			markfloor   = TRUE  ; closed door
			markceiling = TRUE
		}

		; top texture visible?
		if worldhigh < worldtop {
			toptexture = [texture_translation [sidedef .top]]

			if ([linedef .flags] & ML_DONTPEGTOP) != 0 {
				; top of texture at top
				rw_toptexturemid = worldtop
			} else {
				let vtop = R_TextureHeight [sidedef .top]
				vtop = vtop + [backsector .ceilh]

				; bottom of texture
				rw_toptexturemid = vtop - viewz
			}
		}

		; bottom texture visible?
		if worldlow > worldbottom {
			bottomtexture = [texture_translation [sidedef .bottom]]

			if ([linedef .flags] & ML_DONTPEGBOTTOM) != 0 {
				; top of texture at top
				rw_bottomtexturemid = worldtop
			} else {
				rw_bottomtexturemid = worldlow
			}
		}

		rw_toptexturemid    = rw_toptexturemid    + [sidedef .y_offset]
		rw_bottomtexturemid = rw_bottomtexturemid + [sidedef .y_offset]

		; allocate space for masked texture tables
		if [sidedef .mid] > 0 {
			if not no_openings {
				[ds .masked]     = TRUE
				[ds .texcolumns] = R_AllocOpenings numcolumn
			}
		}
	}

	; calculate rw_offset (only needed for textured lines)
	offsetangle = offsetangle >> ANGLEFINESHIFT
	rw_offset = FixedMul hyp [finesine offsetangle]

	distangle = rw_normalangle - rw_angle1
	if distangle < ANG180 {
		rw_offset = - rw_offset
	}

	rw_offset = rw_offset + [sidedef .x_offset] + [seg .offset]

	rw_centerangle = viewangle + ANG90
	rw_centerangle = rw_centerangle - rw_normalangle

	; calculate light table.
	; Use different shading for horizontal / vertical / diagonal.
	R_WallLighting seg [frontsector .light]

	; if a floor / ceiling plane is on the wrong side of the view plane,
	; it is definitely invisible and doesn't need to be marked.
	if [frontsector .floorh] >= viewz {
		markfloor = FALSE
	}

	if [frontsector .ceilpic] != skyflatnum {
		if [frontsector .ceilh] <= viewz {
			markceiling = FALSE
		}
	}

	; calculate incremental stepping values for texture edges
	worldtop    = worldtop    >> (FRACBITS - HEIGHTBITS)
	worldbottom = worldbottom >> (FRACBITS - HEIGHTBITS)
	worldhigh   = worldhigh   >> (FRACBITS - HEIGHTBITS)
	worldlow    = worldlow    >> (FRACBITS - HEIGHTBITS)

	let ctryfrac = centeryfrac >> (FRACBITS - HEIGHTBITS)

	topfrac    = ctryfrac - FixedMul worldtop    rw_scale
	bottomfrac = ctryfrac - FixedMul worldbottom rw_scale
	topstep    =          - FixedMul worldtop    rw_scalestep
	bottomstep =          - FixedMul worldbottom rw_scalestep

	if ref? backsector {
		pixhigh     = ctryfrac - FixedMul worldhigh rw_scale
		pixlow      = ctryfrac - FixedMul worldlow  rw_scale
		pixhighstep =          - FixedMul worldhigh rw_scalestep
		pixlowstep  =          - FixedMul worldlow  rw_scalestep
	}

	; render it
	if markceiling {
		if ref? ceilingplane {
			ceilingplane = R_CheckPlane ceilingplane x1 x2
		}
	}

	if markfloor {
		if ref? floorplane {
			floorplane = R_CheckPlane floorplane x1 x2
		}
	}

	colfunc = [ref R_DrawColumn]

	R_RenderSegLoop x1 x2 [ds .texcolumns]

	; force lines with masked walls to act as silhouette
	if [ds .masked] {
		if not [ds .sil_ceil] {
			[ds .sil_ceil]    = TRUE
			[ds .ceil_height] = FRAC_MIN
		}

		if not [ds .sil_floor] {
			[ds .sil_floor]    = TRUE
			[ds .floor_height] = FRAC_MAX
		}
	}

	; andrewj: don't die if we run out of openings.
	;          instead just disable silhouettes and masked walls.
	;          rendering will be glitched though!
	if no_openings {
		[ds .sil_floor] = FALSE
		[ds .sil_ceil]  = FALSE
	}

	; save sprite clipping info.  needed for masked walls too
	if [ds .sil_ceil] {
		[ds .topclip] = R_AllocOpenings numcolumn
		R_CopyClipBuffer [ds .topclip] ceilingclip x1 numcolumn
	}

	if [ds .sil_floor] {
		[ds .bottomclip] = R_AllocOpenings numcolumn
		R_CopyClipBuffer [ds .bottomclip] floorclip x1 numcolumn
	}
}

;
; R_RenderSegLoop
;
; Draws zero, one, or two textures for walls.
; For masked walls (railings etc) it saves the texture columns.
; Also marks the vertical pixel range of floors and ceilings.
;
fun R_RenderSegLoop (x1 s32, x2 s32, texcolumns ^[0]s16) {
	let x = x1
	loop while x <= x2 {
		; mark floor / ceiling areas
		let cclip = s32 ([ceilingclip x] + 1)
		let fclip = s32 ([floorclip x] - 1)

		let yl = topfrac + (HEIGHTUNIT - 1)
		yl = yl >> HEIGHTBITS
		yl = max yl cclip

		let yh = bottomfrac >> HEIGHTBITS
		yh = min yh fclip

		if markceiling {
			if ref? ceilingplane {
				let top    = cclip
				let bottom = min (yl - 1) fclip

				if top <= bottom {
					[ceilingplane .top    x] = u8 top
					[ceilingplane .bottom x] = u8 bottom
				}
			}
		}

		if markfloor {
			if ref? floorplane {
				let bottom = fclip
				let top    = max (yh + 1) cclip

				if top <= bottom {
					[floorplane .top    x] = u8 top
					[floorplane .bottom x] = u8 bottom
				}
			}
		}

		; texture column and lighting are independent of wall tiers

		let angle = rw_centerangle + [xtoviewangle x]
		angle = angle >> ANGLEFINESHIFT
		angle = angle & 4095

		let texcol = rw_offset - FixedMul [finetangent angle] rw_distance
		texcol = texcol >> FRACBITS

		; calculate lighting
		if ref? fixedcolormap {
			[dc .colormap] = fixedcolormap
		} else {
			let index = rw_scale >> LIGHTSCALESHIFT
			index = max index 0
			index = min index (MAXLIGHTSCALE - 1)

			[dc .colormap] = [scalelight walllight index]
		}

		let iscale = u32 rw_scale
		iscale = 0xffffffff / iscale

		[dc .x] = x
		[dc .iscale] = s32 iscale

		; draw the wall tiers....

		if midtexture != 0 {
			; -- single sided line --

			[dc .yl] = yl
			[dc .yh] = yh
			[dc .texturemid] = rw_midtexturemid
			[dc .source] = R_GetColumn midtexture texcol FALSE

			call colfunc

			[ceilingclip x] = s16 viewheight
			[floorclip   x] = s16 -1
		} else {
			; -- two sided line --

			if toptexture != 0 {
				; top wall
				let mid = pixhigh >> HEIGHTBITS
				mid = min mid fclip

				if mid < yl {
					; mark ceil
					[ceilingclip x] = s16 (yl - 1)
				} else {
					[ceilingclip x] = s16 mid

					[dc .yl] = yl
					[dc .yh] = mid
					[dc .texturemid] = rw_toptexturemid
					[dc .source] = R_GetColumn toptexture texcol FALSE

					call colfunc
				}
			} else if markceiling {
				; mark ceil
				[ceilingclip x] = s16 (yl - 1)
			}

			if bottomtexture != 0 {
				; bottom wall
				let mid = pixlow + (HEIGHTUNIT - 1)
				mid = mid >> HEIGHTBITS
				mid = max mid cclip

				if mid > yh {
					; mark floor
					[floorclip x] = s16 (yh + 1)
				} else {
					[floorclip x] = s16 mid

					[dc .yl] = mid
					[dc .yh] = yh
					[dc .texturemid] = rw_bottomtexturemid
					[dc .source] = R_GetColumn bottomtexture texcol FALSE

					call colfunc
				}
			} else if markfloor {
				; mark floor
				[floorclip x] = s16 (yh + 1)
			}

			if ref? texcolumns {
				; save texture column for masked walls
				let mx = x - x1
				[texcolumns mx] = s16 texcol
			}
		}

		rw_scale   = rw_scale   + rw_scalestep
		topfrac    = topfrac    + topstep
		bottomfrac = bottomfrac + bottomstep
		pixhigh    = pixhigh    + pixhighstep
		pixlow     = pixlow     + pixlowstep

		x = x + 1
	}
}

;
; R_RenderMaskedSegRange
;
; called by sprite drawing code, which handles the depth sorting
; between sprites and masked walls.  the x1/x2 range may be only a
; portion of the seg, those columns are marked as drawn so that any
; future call can skip those columns.
;
fun R_RenderMaskedSegRange (ds ^drawseg_t, x1 s32, x2 s32) {
	let seg = [ds .seg]
	curline = seg

	let frontsector = [seg .front]
	let backsector  = [seg .back]

	let sidedef = [seg .sidedef]
	let linedef = [seg .linedef]

	let midtex = [texture_translation [sidedef .mid]]

	let ystep  = x1 - [ds .x1]
	ystep  = ystep * [ds .scalestep]
	let yscale = [ds .scale1] + ystep

	; find positioning
	let texturemid = fixed_t 0

	if ([linedef .flags] & ML_DONTPEGBOTTOM) != 0 {
		let tex_height = R_TextureHeight midtex
		texturemid = max [frontsector .floorh] [backsector .floorh]
		texturemid = texturemid + tex_height
	} else {
		texturemid = min [frontsector .ceilh] [backsector .ceilh]
	}

	; Use different shading for horizontal / vertical / diagonal.
	R_WallLighting seg [frontsector .light]

	; draw the columns
	let texcolumns = [ds .texcolumns]

	texturemid = texturemid - viewz
	texturemid = texturemid + [sidedef .y_offset]

	msk_x1        = [ds .x1]
	msk_floorclip = [ds .bottomclip]
	msk_ceilclip  = [ds .topclip]

	colfunc = [ref R_DrawColumn]

	let x = x1
	loop while x <= x2 {
		let mx     = x - [ds .x1]
		let texcol = s32 [texcolumns mx]

		if texcol != COLUMN_DRAWN {
			[texcolumns mx] = COLUMN_DRAWN

			; calculate lighting
			if ref? fixedcolormap {
				[dc .colormap] = fixedcolormap
			} else {
				let index = yscale >> LIGHTSCALESHIFT
				index = max index 0
				index = min index (MAXLIGHTSCALE - 1)

				[dc .colormap] = [scalelight walllight index]
			}

			let ytop = centeryfrac - FixedMul texturemid yscale

			let iscale = u32 yscale
			iscale = 0xffffffff / iscale

			[dc .iscale] = s32 iscale

			; draw the texture
			let source = R_GetColumn midtex texcol TRUE
			let posts = cast ^post_t source

			[dc .x] = x
			[dc .texturemid] = texturemid

			R_DrawMaskedColumn posts ytop yscale
		}

		yscale = yscale + [ds .scalestep]
		x = x + 1
	}
}

fun R_WallLighting (seg ^seg_t, light s16) {
	if null? fixedcolormap {
		seg = curline

		let lightnum = s32 light
		lightnum = lightnum >> LIGHTSEGSHIFT + extralight

		if [[seg .v1] .y] == [[seg .v2] .y] {
			lightnum = lightnum - 1
		} else if [[seg .v1] .x] == [[seg .v2] .x] {
			lightnum = lightnum + 1
		}

		lightnum = max lightnum 0
		lightnum = min lightnum (LIGHTLEVELS - 1)

		walllight = lightnum
	}
}

fun R_AllocOpenings (count s32 -> ^[0]s16) {
	let start = numopening
	numopening = numopening + count

	if numopening > MAXOPENINGS {
		I_Error "R_StoreWallRange: opening overflow"
	}

	return [ref openings start]
}

fun R_CopyClipBuffer (dest ^s16, src ^s16, x1 s32, count s32) {
	x1   = x1 * 2
	src  = src + x1
	let size = count * 2

	I_MemCopy dest src (u32 size)
}
