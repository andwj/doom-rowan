;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

const DOORSPEED = 2 * FRACUNIT
const DOORWAIT  = 150

type door_type_e = s32

const dr_normal       = 0
const dr_close30Open  = 1
const dr_close        = 2
const dr_open         = 3
const dr_raiseIn5Min  = 4
const dr_blazeRaise   = 5
const dr_blazeOpen    = 6
const dr_blazeClose   = 7

type door_t = struct {
	.thinker     thinker_t
	.sector     ^sector_t

	.type        door_type_e
	.speed       fixed_t
	.top         fixed_t

	.direction   s32
	.wait        s32   ; tics to wait at the top
	.count       s32   ; countdown when waiting
}

const DIR_WAIT = 0

;
; T_VerticalDoor
;
fun T_VerticalDoor (D ^door_t) {
	let sec  = [D .sector]
	let kind = [D .type]

	if [D .direction] == DIR_WAIT {
		; WAITING
		[D .count] = [D .count] - 1

		if [D .count] == 0 {
			if kind == dr_blazeRaise {
				; time to go back down
				[D .direction] = -1
				S_StartSound [ref sec .soundorg] sfx_bdcls

			} else if kind == dr_normal {
				; time to go back down
				[D .direction] = -1
				S_StartSound [ref sec .soundorg] sfx_dorcls

			} else if kind == dr_close30Open {
				[D .direction] = +1
				S_StartSound [ref sec .soundorg] sfx_doropn

			} else if kind == dr_raiseIn5Min {
				[D .type] = dr_normal
				[D .direction] = +1

				S_StartSound [ref sec .soundorg] sfx_doropn
			}
		}

		return
	}

	if [D .direction] < 0 {
		; DOWN
		let res = T_MoveCeilingPlane [D .sector] [D .speed] [sec .floorh] FALSE -1

		if res == RES_pastdest {
			if matches? kind dr_blazeRaise dr_blazeClose {
				; unlink and free
				P_RemoveThinker (cast ^thinker_t D)
				[sec .specialdata] = NULL

				S_StartSound [ref sec .soundorg] sfx_bdcls

			} else if matches? kind dr_normal dr_close {
				; unlink and free
				P_RemoveThinker (cast ^thinker_t D)
				[sec .specialdata] = NULL

			} else if kind == dr_close30Open {
				[D .direction] = DIR_WAIT
				[D .count]     = TICRATE * 30
			}

		} else if res == RES_crushed {
			if matches? kind dr_close dr_blazeClose {
				; DO NOT GO BACK UP!
			} else {
				[D .direction] = +1
				S_StartSound [ref sec .soundorg] sfx_doropn
			}
		}

		return
	}

	if [D .direction] > 0 {
		; UP
		let res = T_MoveCeilingPlane [D .sector] [D .speed] [D .top] FALSE +1

		if res == RES_pastdest {
			if matches? kind dr_normal dr_blazeRaise {
				; wait at top
				[D .direction] = DIR_WAIT
				[D .count]     = [D .wait]

			} else if matches? kind dr_open dr_blazeOpen dr_close30Open {
				; unlink and free
				P_RemoveThinker (cast ^thinker_t D)
				[sec .specialdata] = NULL
			}
		}

		return
	}
}

;
; EV_DoLockedDoor
; Move a locked door up/down
;
fun EV_DoLockedDoor (ld ^line_t, kind door_type_e, mo ^mobj_t -> bool) {
	let p = [mo .player]

	if null? p {
		return FALSE
	}

	let spec = [ld .special]

	if matches? spec 99 133 {
		; Blue Lock
		let has_key = [p .cards it_bluecard] or [p .cards it_blueskull]

		if not has_key {
			P_PlayerMessage p PD_BLUEO
			S_StartHudSound (cast ^soundmobj_t [p .mo]) sfx_oof
			return FALSE
		}
	}

	if matches? spec 134 135 {
		; Red Lock
		let has_key = [p .cards it_redcard] or [p .cards it_redskull]

		if not has_key {
			P_PlayerMessage p PD_REDO
			S_StartHudSound (cast ^soundmobj_t [p .mo]) sfx_oof
			return FALSE
		}
	}

	if matches? spec 136 137 {
		; Yellow Lock
		let has_key = [p .cards it_yellowcard] or [p .cards it_yellowskull]

		if not has_key {
			P_PlayerMessage p PD_YELLOWO
			S_StartHudSound (cast ^soundmobj_t [p .mo]) sfx_oof
			return FALSE
		}
	}

	return EV_DoDoor ld kind
}

;
;  Normal (tagged) door
;
fun EV_DoDoor (ld ^line_t, kind door_type_e -> bool) {
	let did = FALSE

	let secnum = s32 -1

	loop {
		secnum = P_FindSectorFromLineTag ld secnum
		break if secnum < 0

		let sec = [ref sectors secnum]

		if null? [sec .specialdata] {
			; new door thinker
			let D = cast ^door_t Z_Calloc door_t.size PU_LEVEL

			[D .thinker .func] = THINK_door

			; set default parameters, adjusted below
			[D .sector]    = sec
			[D .type]      = kind
			[D .speed]     = DOORSPEED
			[D .wait]      = DOORWAIT
			[D .direction] = +1
			[D .top]       = P_FindLowestCeilingSurrounding sec
			[D .top] = [D .top] - (4 * FRACUNIT)

			if kind == dr_blazeClose {
				[D .speed]     = DOORSPEED * 4
				[D .direction] = -1
				S_StartSound [ref sec .soundorg] sfx_bdcls

			} else if kind == dr_close {
				[D .direction] = -1
				S_StartSound [ref sec .soundorg] sfx_dorcls

			} else if kind == dr_close30Open {
				[D .top] = [sec .ceilh]
				[D .direction] = -1
				S_StartSound [ref sec .soundorg] sfx_dorcls

			} else if matches? kind dr_blazeRaise dr_blazeOpen {
				[D .speed] = DOORSPEED * 4
				if [D .top] != [sec .ceilh] {
					S_StartSound [ref sec .soundorg] sfx_bdopn
				}

			} else if matches? kind dr_normal dr_open {
				if [D .top] != [sec .ceilh] {
					S_StartSound [ref sec .soundorg] sfx_doropn
				}
			}

			P_AddThinker (cast ^thinker_t D)
			[sec .specialdata] = D

			did = TRUE
		}
	}

	return did
}

;
; EV_ManualDoor : open a door manually, no tag value
;
fun EV_ManualDoor (ld ^line_t, mo ^mobj_t -> bool) {
	let p    = [mo .player]
	let spec = [ld .special]

	; check for locks...

	if matches? spec 26 32 {
		; Blue Lock

		if null? p {
			return FALSE
		}

		let has_key = [p .cards it_bluecard] or [p .cards it_blueskull]

		if not has_key {
			P_PlayerMessage p PD_BLUEK
			S_StartHudSound (cast ^soundmobj_t [p .mo]) sfx_oof
			return FALSE
		}
	}

	if matches? spec 28 33 {
		; Red Lock

		if null? p {
			return FALSE
		}

		let has_key = [p .cards it_redcard] or [p .cards it_redskull]

		if not has_key {
			P_PlayerMessage p PD_REDK
			S_StartHudSound (cast ^soundmobj_t [p .mo]) sfx_oof
			return FALSE
		}
	}

	if matches? spec 27 34 {
		; Yellow Lock

		if null? p {
			return FALSE
		}

		let has_key = [p .cards it_yellowcard] or [p .cards it_yellowskull]

		if not has_key {
			P_PlayerMessage p PD_YELLOWK
			S_StartHudSound (cast ^soundmobj_t [p .mo]) sfx_oof
			return FALSE
		}
	}

	; only front sides can be used

	if [ld .sidenum 1] < 0 {
		; andrewj: ignore this instead of bombing out
		; I_Error("EV_ManualDoor: DR special type on 1-sided linedef")
		return FALSE
	}

	; if the sector has an active thinker, use it
	let sec = [ld .back]

	let th = cast ^thinker_t [sec .specialdata]
	if ref? th {
		; ONLY FOR "RAISE" DOORS, NOT "OPEN"s
		if matches? spec 1 26 27 28 117 {
			let is_plat = [th .func] == THINK_plat
			let is_door = [th .func] == THINK_door

			; When is a door not a door?
			; In Vanilla, door->direction is set, even though
			; "specialdata" might not actually point at a door.

			if is_plat {
				let P = cast ^plat_t [sec .specialdata]

				if null? p {
					return FALSE
				}

				; The direction field in door_t corresponds to the wait
				; field in plat_t.  Let's set that to -1 instead.

				[P .wait] = -1

			} else if is_door {
				let D = cast ^door_t [sec .specialdata]

				if [D .direction] < 0 {
					; go back up
					[D .direction] = +1
				} else if null? p {
					; JDC: bad guys never close doors
					return FALSE
				} else {
					; start going down immediately
					[D .direction] = -1
				}

			} else {
				; This isn't a door OR a plat.  Now we're in trouble.
				I_Print "EV_ManualDoor: Tried to close a non-door.\n"
			}

			return TRUE
		}
	}

	; for proper sound
	let sfx = sfxenum_t sfx_doropn

	if matches? spec 117 118 {
		; BLAZING DOOR
		sfx = sfx_bdopn
	}

	S_StartSound [ref sec .soundorg] sfx

	; new door thinker
	let D = cast ^door_t Z_Calloc door_t.size PU_LEVEL

	[D .thinker .func] = THINK_door

	; set default parameters, adjusted below
	[D .sector]    = sec
	[D .type]      = dr_normal
	[D .speed]     = DOORSPEED
	[D .wait]      = DOORWAIT
	[D .direction] = +1

	if spec >= 31 and spec <= 34 {
		[D .type] = dr_open
		[ld .special] = 0

	} else if spec == 117 {
		; blazing door raise
		[D .type]  = dr_blazeRaise
		[D .speed] = DOORSPEED * 4

	} else if spec == 118 {
		; blazing door open
		[D .type]  = dr_blazeOpen
		[D .speed] = DOORSPEED * 4
		[ld .special] = 0
	}

	; find the top of the movement range
	[D .top] = P_FindLowestCeilingSurrounding sec
	[D .top] = [D .top] - (4 * FRACUNIT)

	P_AddThinker (cast ^thinker_t D)
	[sec .specialdata] = D

	return TRUE
}

;
; Spawn a door that closes after 30 seconds
;
fun P_SpawnDoorCloseIn30 (sec ^sector_t) {
	let D = cast ^door_t Z_Calloc door_t.size PU_LEVEL

	[D .thinker .func] = THINK_door

	[D .sector]    = sec
	[D .type]      = dr_normal
	[D .speed]     = DOORSPEED
	[D .top]       = 0
	[D .direction] = DIR_WAIT
	[D .wait]      = DOORWAIT
	[D .count]     = 30 * TICRATE

	P_AddThinker (cast ^thinker_t D)

	[sec .specialdata] = D
	[sec .special] = 0
}

;
; Spawn a door that opens after 5 minutes
;
fun P_SpawnDoorRaiseIn5Mins (sec ^sector_t) {
	let D = cast ^door_t Z_Calloc door_t.size PU_LEVEL

	[D .thinker .func] = THINK_door

	[D .sector]    = sec
	[D .type]      = dr_raiseIn5Min
	[D .speed]     = DOORSPEED
	[D .top]       = P_FindLowestCeilingSurrounding sec - (4 * FRACUNIT)
	[D .direction] = DIR_WAIT
	[D .wait]      = DOORWAIT
	[D .count]     = 300 * TICRATE

	P_AddThinker (cast ^thinker_t D)

	[sec .specialdata] = D
	[sec .special] = 0
}
