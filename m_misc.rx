;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

; increment every time a check is made
var validcount s32 = 1

;;
;; Determine the length of an open file.
;;
fun M_FileLength (handle ^FILE -> s32) {
	; save the current position in the file
	let savedpos = I_Ftell handle

	; jump to the end to find the length
	I_Fseek handle 0 SEEK_END

	let length = I_Ftell handle

	; go back to the old location
	I_Fseek handle savedpos SEEK_SET

	return length
}

fun M_WriteFile (name ^uchar, buffer ^raw-mem, length s32 -> bool) {
	let handle = I_Fopen name "wb"

	if null? handle {
		return FALSE
	}

	let count = I_Fwrite buffer length handle
	I_Fclose handle

	return count >= length
}

;; Check if a file exists by probing for common case variation of its filename.
;; Returns a newly allocated string that the caller is responsible for freeing.

fun M_FileCaseExists (path ^uchar -> ^uchar) {
	path = M_StringDuplicate path

	; transform directory separators to Unixy ones
	let s = path
	loop {
		let ch = [s]
		break if ch == 0
		if ch == '\\' {
			[s] = '/'
		}
		s = s + 1
	}

	; 0 - the given path
	let exist = I_FileOrDirExists path
	if exist != 0 {
		return path
	}

	let basename = path + (M_StrLen path)
	basename = basename - 1

	loop {
		break if basename <= path
		break if [basename] == '/'

		basename = basename - 1
	}

	if [basename] == '/' {
		basename = basename + 1
	}

	; 1 - lowercase filename, e.g. doom2.wad
	M_ForceLowercase basename

	exist = I_FileOrDirExists path
	if exist != 0 {
		return path
	}

	; 2 - uppercase filename, e.g. DOOM2.WAD
	M_ForceUppercase basename

	exist = I_FileOrDirExists path
	if exist != 0 {
		return path
	}

	; 3 - uppercase basename with lowercase extension, e.g. DOOM2.wad
	let ext = basename + (M_StrLen basename)
	ext = ext - 1

	loop {
		break if ext <= basename
		break if [ext] == '.'

		ext = ext - 1
	}

	if ext > basename {
		M_ForceLowercase ext

		exist = I_FileOrDirExists path
		if exist != 0 {
			return path
		}
	}

	; 4 - lowercase filename with uppercase first letter, e.g. Doom2.wad
	if M_StrLen basename > 1 {
		M_ForceLowercase (basename + 1)

		exist = I_FileOrDirExists path
		if exist != 0 {
			return path
		}
	}

	; 5 - no luck
	I_Free path
	return NULL
}

fun M_ExtractFileBase (path ^uchar, dest ^[8]uchar) {
	I_MemSet dest 0 8

	let src = path + M_StrLen path
	src = src - 1

	; back up until a backslash or the start
	loop {
		if src <= path {
			jump truncate
		}

		let ch = [src]

		break if ch == '/'
		break if ch == '\\'

		src = src - 1
	}

	src = src + 1

	do truncate {
		let filename = src

		; Copy up to eight characters
		; Note: Vanilla Doom exits with an error if a filename is specified
		; with a base of more than eight characters.  To remove the 8.3
		; filename limit, instead we simply truncate the name.

		let length = u32 0

		loop {
			let ch = [src]

			break if ch == 0
			break if ch == '.'

			if length >= 8 {
				I_Print2 "Warning: Truncated '%s' lump name\n" filename
				break
			}

			[dest length] = M_ToUpper ch
			length = length + 1
		}
	}
}

fun M_Atoi (str ^uchar -> s32) {
	let result = s32 0

	; skip whitespace
	loop while [str] == ' ' {
		str = str + 1
	}

	let is_neg = [str] == '-'
	if is_neg {
		str = str + 1
	}

	loop {
		let ch = [str]
		let is_digit = ch >= '0' and ch <= '9'

		break unless is_digit

		let digit = s32 (ch - '0')
		result = result * 10
		result = result + digit

		str = str + 1
	}

	if is_neg {
		result = - result
	}

	return result
}

fun M_Itoa (buf ^uchar, value s32) {
	if value < 0 {
		[buf] = '-'
		buf = buf + 1
		value = - value
	}

	let t = s32 1000000000
	let seen_digit = FALSE

	loop until t == 0 {
		if value >= t or seen_digit {
			let digit = value / t
			value = value - (digit * t)

			if digit > 0 {
				seen_digit = TRUE
			}

			[buf] = '0' + uchar digit
			buf = buf + 1
		}

		t = t / 10
	}

	if not seen_digit {
		[buf] = '0'
		buf = buf + 1
	}

	[buf] = 0
}

fun M_ForceUppercase (p ^uchar) {
	loop {
		let ch = [p]
		break if ch == 0
		[p] = M_ToUpper ch
		p = p + 1
	}
}

fun M_ForceLowercase (p ^uchar) {
	loop {
		let ch = [p]
		break if ch == 0
		[p] = M_ToLower ch
		p = p + 1
	}
}

;;
;; Safe version of strdup() that checks the string was successfully
;; allocated.
;;
fun M_StringDuplicate (orig ^uchar -> ^uchar) {
	let len = M_StrLen orig
	len = len + 1

	let result = cast ^uchar I_Alloc len
	I_MemCopy result orig len

	return result
}

;; Return a newly-malloced string with all the strings given as arguments
;; concatenated together.

fun M_StringJoin (A ^uchar, B ^uchar -> ^uchar) {
	return M_StringJoin3 A B ""
}

fun M_StringJoin3 (A ^uchar, B ^uchar, C ^uchar -> ^uchar) {
	let A_len = M_StrLen A
	let B_len = M_StrLen B
	let C_len = M_StrLen C

	let new_len = A_len + B_len
	new_len = new_len + C_len
	new_len = new_len + 1

	let result = cast ^uchar I_Alloc new_len

	I_MemCopy result A A_len

	let B_store = result + A_len

	I_MemCopy B_store B B_len

	let C_store = B_store + B_len
	C_len   = C_len + 1   ; trailing NUL

	I_MemCopy C_store C C_len
	return result
}

;; Safe string copy function that works like OpenBSD's strlcpy().
;; Returns true if the string was not truncated.

fun M_StringCopy (dest ^uchar, src ^uchar, destsize u32 -> bool) {
	let len = M_StrLen src
	len = len + 1

	if len < destsize {
		I_MemCopy dest src len
		return TRUE
	}

	if destsize > 0 {
		destsize = destsize - 1
		I_MemCopy dest src destsize

		dest = dest + destsize
		[dest] = 0
	}

	return FALSE
}

;; Safe string concat function that works like OpenBSD's strlcat().
;; Returns true if string not truncated.

fun M_StringConcat (dest ^uchar, src ^uchar, destsize u32 -> bool) {
	let offset = M_StrLen dest

	if offset > destsize {
		offset = destsize
	}

	dest = dest + offset
	destsize = destsize - offset

	return M_StringCopy dest src destsize
}

;; Returns true if 's' begins with the specified prefix.

fun M_StringStartsWith (s ^uchar, prefix ^uchar -> bool) {
	let s_len = M_StrLen s
	let p_len = M_StrLen prefix

	if s_len <= p_len {
		return FALSE
	}

	return M_StrNCmp s prefix p_len == 0
}

;; Returns true if 's' ends with the specified suffix.

fun M_StringEndsWith (s ^uchar, suffix ^uchar -> bool) {
	let s_len = M_StrLen s
	let p_len = M_StrLen suffix

	if s_len <= p_len {
		return FALSE
	}

	s = s + s_len
	s = s - p_len

	return M_StrNCmp s suffix p_len == 0
}

; andrewj: added this, quite simplistic
fun M_FormatNum (buf ^uchar, buflen u32, s ^uchar arg s32) {
	let number = stack-var [64]uchar
	M_Itoa number arg

	loop {
		let ch = [s]

		break if ch == 0
		break if buflen <= 1

		let next = [(s + 1)]
		let is_fmt = ch == '%' and next == 'd'

		if is_fmt {
			let n = cast ^uchar number

			loop {
				ch = [n]

				break if ch == 0
				break if buflen <= 1

				[buf]  = ch

				n = n + 1
				buf = buf + 1
				buflen = buflen - 1
			}

			s = s + 2
		} else {
			[buf]  = ch

			s = s + 1
			buf = buf + 1
			buflen = buflen - 1
		}
	}

	[buf] = 0
}

fun M_StrCmp (A ^uchar, B ^uchar -> s32) {
	loop {
		let AC = [A]
		let BC = [B]

		if AC | BC == 0 {
			return 0
		}

		; this test also catches end-of-string conditions
		if AC != BC {
			return s32 AC - s32 BC
		}

		A = A + 1
		B = B + 1
	}
}

fun M_StrNCmp (A ^uchar, B ^uchar n u32 -> s32) {
	loop {
		let AC = [A]
		let BC = [B]

		if n == 0 {
			return 0
		}

		if AC | BC == 0 {
			return 0
		}

		; this test also catches end-of-string conditions
		if AC != BC {
			return s32 AC - s32 BC
		}

		A = A + 1
		B = B + 1
		n = n - 1
	}
}

fun M_StrCaseCmp (A ^uchar, B ^uchar -> s32) {
	loop {
		let AC = M_ToUpper [A]
		let BC = M_ToUpper [B]

		if AC | BC == 0 {
			return 0
		}

		; this test also catches end-of-string conditions
		if AC != BC {
			return s32 AC - s32 BC
		}

		A = A + 1
		B = B + 1
	}
}

fun M_StrNCaseCmp (A ^uchar, B ^uchar, n u32 -> s32) {
	loop {
		let AC = M_ToUpper [A]
		let BC = M_ToUpper [B]

		if n == 0 {
			return 0
		}

		if AC | BC == 0 {
			return 0
		}

		; this test also catches end-of-string conditions
		if AC != BC {
			return s32 AC - s32 BC
		}

		A = A + 1
		B = B + 1
		n = n - 1
	}
}

fun M_StrLen (s ^uchar -> u32) {
	let count = u32 0

	loop until [s] == 0 {
		s = s + 1
		count = count + 1
	}

	return count
}

fun M_ToLower (ch uchar -> uchar) {
	let is_upper = ch >= 'A' and ch <= 'Z'

	if is_upper {
		ch = ch + 32
	}

	return ch
}

fun M_ToUpper (ch uchar -> uchar) {
	let is_lower = ch >= 'a' and ch <= 'z'

	if is_lower {
		ch = ch - 32
	}

	return ch
}

fun M_IsSpace (ch uchar -> bool) {
	; this is fairly lax
	if ch > 0 and ch <= ' ' {
		return TRUE
	}

	return FALSE
}

;
; Hash function used for lump names and texture names.
;
fun M_HashName8 (s ^uchar, total u32 -> u32) {
	; This is the djb2 string hash function, modded to work on strings
	; that have a maximum length of 8.

	if total == 0 {
		return 0
	}

	let result = u32 5381

	let i = u32 0
	loop while i < 8 {
		let ch = [s]
		break if ch == 0

		ch = M_ToUpper ch

		result = result ~ (result << 5)
		result = result ~ (u32 ch)

		i = i + 1
		s = s + 1
	}

	result = result % total
	return result
}

fun M_HasExtension (filename ^uchar, ext ^uchar -> bool) {
	let len = M_StrLen filename
	if len == 0 {
		return FALSE
	}

	let f = filename + len

	loop {
		f = f - 1

		if f <= filename {
			return FALSE
		}

		if [f] == '.' {
			f = f + 1
			return M_StrCaseCmp f ext == 0
		}
	}
}
