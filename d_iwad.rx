;;
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

type iwad_t = struct {
	.name        ^uchar
	.mission      GameMission_t
	.mode         GameMode_t
	.description ^uchar
}

const NUM_IWADS = 10

rom-var iwads [NUM_IWADS]iwad_t = {
	{ "doom2.wad"      mi_doom2      commercial  "Doom II" }
	{ "plutonia.wad"   mi_plutonia   commercial  "Final Doom: Plutonia Experiment" }
	{ "tnt.wad"        mi_tnt        commercial  "Final Doom: TNT: Evilution" }
	{ "doom.wad"       mi_doom       retail      "Doom" }
	{ "doom1.wad"      mi_doom       shareware   "Doom Shareware" }
	{ "chex.wad"       mi_chex       retail      "Chex Quest" }
	{ "hacx.wad"       mi_hacx       commercial  "Hacx" }
	{ "freedm.wad"     mi_doom2      commercial  "FreeDM" }
	{ "freedoom2.wad"  mi_doom2      commercial  "Freedoom: Phase 2" }
	{ "freedoom1.wad"  mi_doom       retail      "Freedoom: Phase 1" }
}

; Array of locations to search for IWAD files
;
; "128 IWAD search directories should be enough for anybody".
const MAX_IWAD_DIRS = 128

zero-var iwad_dirs [MAX_IWAD_DIRS]^uchar
var      iwad_dir_num s32 = 0
var      iwad_dirs_built bool = FALSE

fun AddIWADDir (dir ^uchar) {
	if iwad_dir_num < MAX_IWAD_DIRS {
		[iwad_dirs iwad_dir_num] = dir
		iwad_dir_num = iwad_dir_num + 1
	}
}

; Returns true if the specified path is a path to a file
; of the specified name.

fun DirIsFile (path ^[0]uchar, name ^uchar -> bool) {
	let path_len = M_StrLen path
	let name_len = M_StrLen name

	if path_len <= name_len {
		return FALSE
	}

	let pos = path_len - name_len
	pos = pos - 1
	let ch  = [path pos]

	if matches? ch '/' '\\' {
		pos = pos + 1
		return M_StrCaseCmp [ref path pos] name == 0
	}

	return FALSE
}

; Check if the specified directory contains the specified IWAD
; file, returning the full path to the IWAD if found, or NULL
; if not found.

fun CheckDirectoryHasIWAD (dir ^uchar, iwadname ^uchar -> ^uchar) {
	; As a special case, the "directory" may refer directly to an
	; IWAD file if the path comes from DOOMWADDIR or DOOMWADPATH.

	let probe = M_FileCaseExists dir
	if ref? probe {
		if DirIsFile dir iwadname {
			return probe
		}
	}

	; Construct the full path to the IWAD if it is located in
	; this directory, and check if it exists.

	let filename = ^uchar NULL

	if M_StrCmp dir "." == 0 {
		filename = M_StringDuplicate iwadname
	} else {
		filename = M_StringJoin3 dir "/" iwadname
	}

	probe = M_FileCaseExists filename
	if ref? probe {
		return probe
	}

	I_Free filename

	return NULL
}

; Search a directory to try to find an IWAD
; Returns the location of the IWAD if found, otherwise NULL.

fun SearchDirectoryForIWAD (dir ^uchar, mission ^GameMission_t -> ^uchar) {
	let i = s32 0
	loop while i < NUM_IWADS {
		let filename = CheckDirectoryHasIWAD dir [iwads i .name]

		if ref? filename {
			[mission] = [iwads i .mission]
			return filename
		}

		i = i + 1
	}

	return NULL
}

; When given an IWAD with the '-iwad' parameter,
; attempt to identify it by its name.

fun IdentifyIWADByName (name ^uchar -> GameMission_t) {
	let p = name + (M_StrLen name)

	loop {
		break if p <= name

		p = p - 1
		let ch = [p]

		if matches? ch '/' '\\' {
			p = p + 1
			break
		}
	}

	let i = s32 0
	loop while i < NUM_IWADS {
		; Check if the filename is this IWAD name.

		if M_StrCaseCmp p [iwads i .name] == 0 {
			return [iwads i .mission]
		}

		i = i + 1
	}

	return mi_unknown
}

;
; Build a list of IWAD files
;

fun BuildIWADDirList () {
	if iwad_dirs_built {
		return
	}

	; Look in the current directory.  Doom always does this.
	AddIWADDir "."

	; Add DOOMWADDIR if it is in the environment
	let env = I_Getenv "DOOMWADDIR"
	if ref? env {
		AddIWADDir env
	}

	; Don't run this function again.
	iwad_dirs_built = TRUE
}

;
; Searches WAD search paths for an WAD with a specific filename.
;

fun D_FindWADByName (name ^uchar -> ^uchar) {
	; Absolute path?

	let probe = M_FileCaseExists name
	if ref? probe {
		return probe
	}

	BuildIWADDirList

	; Search through all IWAD paths for a file with the given name.

	let i = s32 0
	loop while i < iwad_dir_num {
		; As a special case, if this is in DOOMWADDIR or DOOMWADPATH,
		; the "directory" may actually refer directly to an IWAD file.

		probe = M_FileCaseExists [iwad_dirs i]
		if ref? probe {
			if DirIsFile [iwad_dirs i] name {
				return probe
			}
		}

		; Construct a string for the full path

		let path  = M_StringJoin3 [iwad_dirs i] "/" name

		probe = M_FileCaseExists path
		if ref? probe {
			return probe
		}

		I_Free path

		i = i + 1
	}

	; File not found
	return NULL
}

#public

;
; D_TryWADByName
;
; Searches for a WAD by its filename, or passes through the filename
; if not found.
;

fun D_TryFindWADByName (filename ^uchar -> ^uchar) {
	let result = D_FindWADByName filename

	if ref? result {
		return result
	}

	return filename
}

;
; FindIWAD
;
; Checks availability of IWAD files by name,
; to determine whether registered/commercial features
; should be executed (notably loading PWADs).
;

fun D_FindIWAD (mission ^GameMission_t -> ^uchar) {
	;
	; specify an IWAD file to use.
	;
	let iwadparm = M_CheckParmWithArgs "-iwad" 1

	if iwadparm > 0 {
		; Search through IWAD dirs for an IWAD with the given name.

		iwadparm = iwadparm + 1
		let iwadfile = [myargv iwadparm]

		let result = D_FindWADByName iwadfile

		if null? result {
			I_Error2 "IWAD file '%s' not found!" iwadfile
		}

		[mission] = IdentifyIWADByName result
		return result
	}

	; Search through the list and look for an IWAD

	BuildIWADDirList

	let i = s32 0
	loop while i < iwad_dir_num {
		let result = SearchDirectoryForIWAD [iwad_dirs i] mission

		if ref? result {
			return result
		}

		i = i + 1
	}

	return NULL
}
