#
#  Requires GNU make.
#

PROGRAM=doom-rowan

ROWAN=rowan
SAGELEAF=sageleaf
NASM=nasm -f elf64 -g

MISC=-std=c99
WARNINGS=-Wall -Wextra -Wno-implicit-fallthrough -Wno-sign-compare \
         -Wno-shadow -Wno-unused-parameter -Wno-unused-function
OPTIMISE=-O1

# default flags for compiler, preprocessor and linker
CFLAGS ?= $(MISC) $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS ?= $(OPTIMISE)
LIBS ?=

BUILD_DIR=_build

DUMMY=$(BUILD_DIR)/zzdummy

#----- Libraries ----------------------------------------------

CPPFLAGS += -I/usr/include/SDL2

LIBS += -lSDL2 -lSDL2_mixer

#----- Object files ----------------------------------------------

all: $(DUMMY) $(PROGRAM)

OBJS = \
	$(BUILD_DIR)/c/i_input.o     \
	$(BUILD_DIR)/c/i_main.o      \
	$(BUILD_DIR)/c/i_midifile.o  \
	$(BUILD_DIR)/c/i_mus2mid.o   \
	$(BUILD_DIR)/c/i_oplmusic.o  \
	$(BUILD_DIR)/c/i_pcx.o   \
	$(BUILD_DIR)/c/i_sdlsound.o  \
	$(BUILD_DIR)/c/i_sound.o   \
	$(BUILD_DIR)/c/i_system.o  \
	$(BUILD_DIR)/c/i_timer.o   \
	$(BUILD_DIR)/c/i_video.o   \
	$(BUILD_DIR)/c/opl3.o      \
	$(BUILD_DIR)/c/opl_queue.o  \
	$(BUILD_DIR)/c/opl_sdl.o   \
	\
	$(BUILD_DIR)/rx/engine.o

$(BUILD_DIR)/rx/engine.ll: *.rx
	$(ROWAN) Build.list

$(BUILD_DIR)/rx/engine.s: $(BUILD_DIR)/rx/engine.ll
	$(SAGELEAF) $< -o $@

$(BUILD_DIR)/rx/%.o: $(BUILD_DIR)/rx/%.s
	$(NASM) $< -o $@

$(BUILD_DIR)/c/%.o: ./%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

# temporary, to help debugging
# .PRECIOUS: $(BUILD_DIR)/rx/m_random.s

#----- Targets -----------------------------------------------

clean:
	rm -f $(PROGRAM) $(BUILD_DIR)/*/*.*
	rm -f core core.* ERRS

rxclean:
	rm -f $(BUILD_DIR)/rx/*.*

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

# this is used to create the BUILD_DIR directory
$(DUMMY):
	mkdir -p $(BUILD_DIR)/c
	mkdir -p $(BUILD_DIR)/rx
	@touch $@

.PHONY: all clean rxclean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
