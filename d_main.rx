;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

; Set if homebrew PWAD stuff has been added.
zero-var modifiedgame  bool

zero-var autostart     bool
zero-var startskill    skill_t
zero-var startepisode  s32
zero-var startmap      s32
zero-var startloadgame s32

zero-var advancedemo bool

; If true, the main game loop has started.
zero-var main_loop_started bool

var show_endoom s32 = 1

var gamedescription ^uchar = NULL

;
; D_ProcessEvents
; Send all the events of the given timestamp down the responder chain
;
fun D_ProcessEvents () {
	loop {
		let ev = D_PopEvent
		break if null? ev

		if M_Responder ev {
			; menu ate the event
		} else {
			G_Responder ev
		}
	}
}

#private

;
; D_Display
; draw current display, possibly wiping it from the previous.
;

type DisplayState_t = struct {
	; wipegamestate can be set to -1 to force a wipe on the next draw
	.wipe    gamestate_t
	.oldgame gamestate_t

	.view    bool
	.menu    bool
	.help    bool

	.fullscreen  bool
	.border      s32
}

zero-var dpy_state DisplayState_t

fun D_InitDisplayState () {
	[dpy_state .wipe]    = GS_DEMOSCREEN
	[dpy_state .oldgame] = -1
}

fun D_Display () {
	; for comparative timing / profiling
	if nodrawers {
		; andrewj: need this to prevent mouse grab for -timedemo -nodraw
		I_UpdateGrab
		return
	}

	; change the view size if needed
	if setsizeneeded {
		R_ExecuteSetViewSize

		[dpy_state .oldgame] = -1   ; force background redraw
		[dpy_state .border]  = 3
	}

	; save the current screen if about to wipe
	let wipe = gamestate != [dpy_state .wipe]
	if wipe {
		F_StartWipe 0 0 SCREENWIDTH SCREENHEIGHT
	}

	let is_full = viewheight == SCREENHEIGHT

	; do buffered drawing...

	if gamestate == GS_LEVEL {
		if gametic == 0 {
			jump done
		}

		if automapactive {
			AM_Drawer
		}

		ST_Drawer is_full

		; do red-/gold-shifts from damage/items
		V_PaletteStuff

	} else if gamestate == GS_INTERMISSION {
		WI_Drawer

	} else if gamestate == GS_FINALE {
		F_Drawer

	} else if gamestate == GS_DEMOSCREEN {
		D_PageDrawer
	}

	do done {}

	; draw buffered stuff to screen
	I_UpdateNoBlit

	; draw the view directly
	if gamestate == GS_LEVEL {
		if gametic != 0 {
			if not automapactive {
				let pl = [ref players displayplayer]
				R_RenderPlayerView pl
			}

			HU_Drawer
		}
	}

	; ensure we have normal palette on gamestate changes
	if gamestate != [dpy_state .oldgame] {
		let pal = W_CacheLumpName "PLAYPAL"
		I_SetPalette pal
	}

	if gamestate == GS_LEVEL {
		; see if the border needs to be initially drawn
		if [dpy_state .oldgame] != GS_LEVEL {
			[dpy_state .view] = FALSE     ; view was not active
			V_FillBackScreen    ; draw the pattern into the back screen
		}

		; see if the border needs to be updated to the screen
		if not automapactive and viewwidth != SCREENWIDTH {
			if menuactive {
				[dpy_state .border] = 3
			} else if [dpy_state .menu] {
				[dpy_state .border] = 3
			} else if not [dpy_state .view] {
				[dpy_state .border] = 3
			}

			if [dpy_state .border] > 0 {
				V_DrawViewBorder   ; erase old menu stuff

				[dpy_state .border] = [dpy_state .border] - 1
			}
		}
	}

	[dpy_state .oldgame] = gamestate
	[dpy_state .wipe]    = gamestate
	[dpy_state .menu]    = menuactive
	[dpy_state .view]    = viewactive
	[dpy_state .help]    = inhelpscreens
	[dpy_state .fullscreen] = is_full

	; draw pause pic
	if paused {
		let y = s32 4

		if not automapactive {
			y = viewwindowy + 4
		}

		let x = viewwidth - 68
		x = viewwindowx + (x / 2)

		let pause_img = W_CacheLumpName "M_PAUSE"

		V_DrawPatch x y pause_img
	}

	M_Drawer     ; menu is drawn even on top of everything
	NetUpdate    ; send out any new accumulation

	; normal update...
	if not wipe {
		I_FinishUpdate    ; page flip or blit buffer
		return
	}

	; wipe update...
	F_EndWipe 0 0 SCREENWIDTH SCREENHEIGHT

	let last_wipe_time = I_GetTime
	last_wipe_time = last_wipe_time - 1

	loop {
		let now_time = s32 0
		let tics     = s32 0

		loop {
			now_time = I_GetTime
			tics = now_time - last_wipe_time
			I_Sleep 1
			break if tics > 0
		}

		last_wipe_time = now_time

		; andrewj: faster wipes when doing a timed demo
		if timingdemo {
			tics = tics * 4
		}

		let wipe_done = F_DrawWipe 0 0 SCREENWIDTH SCREENHEIGHT tics

		I_UpdateNoBlit
		M_Drawer         ; menu is drawn even on top of wipes
		I_FinishUpdate   ; page flip or blit buffer

		break if wipe_done
	}

	F_ResetWipe
}

;
; Add configuration file variable bindings.
;

fun D_BindVariables () {
	I_BindInputVariables
	I_BindVideoVariables
	I_BindSoundVariables

	M_BindBaseControls
	M_BindWeaponControls
	M_BindMapControls
	M_BindMenuControls

	M_BindIntVariable "mouse_sensitivity"   [ref mouseSensitivity]
	M_BindIntVariable "sfx_volume"          [ref sfxVolume]
	M_BindIntVariable "music_volume"        [ref musicVolume]
	M_BindIntVariable "show_messages"       [ref showMessages]
	M_BindIntVariable "screenblocks"        [ref screenblocks]
	M_BindIntVariable "snd_channels"        [ref snd_channels]
	M_BindIntVariable "vanilla_demo_limit"  [ref vanilla_demo_limit]
	M_BindIntVariable "show_endoom"         [ref show_endoom]
}

;
; D_DoomLoop
;
; Not a globally visible function,
;   just included for source reference,
;   called by D_DoomMain, never exits.
; Manages timing and IO,
;   calls all ?_Responder, ?_Ticker, and ?_Drawer,
;   calls I_GetTime, I_StartFrame, and I_StartTic.
;
fun D_DoomLoop (-> no-return) {
	if gamevariant == gv_bfgedition {
		let check = gameaction == ga_playdemo
		check = check or demorecording or netgame
		if check {
			I_Print " WARNING: You are playing using one of the Doom Classic\n"
			I_Print " IWAD files shipped with the Doom 3: BFG Edition. These are\n"
			I_Print " known to be incompatible with the regular IWAD files and\n"
			I_Print " may cause demos and network games to get out of sync.\n"
		}
	}

	if demorecording {
		G_BeginRecording
	}

	main_loop_started = TRUE

	I_SetWindowTitle gamedescription
	I_GraphicsCheckCommandLine

	let doompal = W_CacheLumpName "PLAYPAL"
	I_InitGraphics doompal

	TryRunTics

	V_RestoreBuffer
	R_ExecuteSetViewSize

	D_StartGameLoop

	loop {
		; frame syncronous IO operations
		I_StartFrame

		; this will run at least one tic
		TryRunTics

		; move positional sounds
		let listener = [players consoleplayer .mo]
		S_UpdateSounds (cast ^soundmobj_t listener)

		; update display, next frame, with current state.
		if screenvisible {
			D_Display
		}
	}
}

;;
;;  DEMO LOOP
;;

zero-var demosequence s32

zero-var pagetic   s32
zero-var pagename ^uchar

;
; D_PageDrawer
;
fun D_PageDrawer () {
	let img = W_CacheLumpName pagename
	V_DrawPatch 0 0 img
}

#public

;
; D_WantGrabMouse
;
; Called to determine whether to grab the mouse pointer
;
fun D_WantGrabMouse (-> bool) {
	; when menu is active or game is paused, release the mouse.
	; only grab mouse when playing levels (but not demos).

	if menuactive {
		return FALSE
	} else if paused {
		return FALSE
	} else if demoplayback {
		return FALSE
	} else if advancedemo {
		return FALSE
	}

	return gamestate == GS_LEVEL
}

fun D_ForceWipe () {
	[dpy_state .wipe] = -1
}

;
; D_PageTicker
; Handles timing for warped projection
;
fun D_PageTicker () {
	pagetic = pagetic - 1

	if pagetic < 0 {
		D_AdvanceDemo
	}
}

;
; D_AdvanceDemo
; Called after each demo or intro demosequence finishes
;
fun D_AdvanceDemo () {
	advancedemo = TRUE
}

;
; This cycles through the demo sequences.
; Fixme - version dependent demo numbers?
;
fun D_DoAdvanceDemo () {
	[players consoleplayer .playerstate] = PST_LIVE  ; not reborn

	advancedemo = FALSE
	usergame    = FALSE   ; no save / end game here
	paused      = FALSE
	gameaction  = ga_nothing

	; andrewj: new logic, always play DEMO4 if it exists.
	let demo4_lump = W_CheckNumForName "DEMO4"

	let modulo = s32 6
	if demo4_lump >= 0 {
		modulo = s32 8
	}

	demosequence = demosequence + 1
	demosequence = demosequence % modulo

	if demosequence == 0 {
		gamestate = GS_DEMOSCREEN
		pagename  = "TITLEPIC"

		if gamemode == commercial {
			pagetic = 385
			S_StartMusic mus_dm2ttl
		} else {
			pagetic = 170
			S_StartMusic mus_intro
		}

	} else if demosequence == 1 {
		G_DeferedPlayDemo "demo1"

	} else if demosequence == 2 {
		gamestate = GS_DEMOSCREEN
		pagename  = "CREDIT"
		pagetic   = 200

	} else if demosequence == 3 {
		G_DeferedPlayDemo "demo2"

	} else if demosequence == 4 {
		gamestate = GS_DEMOSCREEN

		if gamemode == commercial {
			pagename = "TITLEPIC"
			pagetic  = 385
			S_StartMusic mus_dm2ttl
		} else {
			if gameversion >= exe_ultimate {
				pagename = "CREDIT"
			} else {
				pagename = "HELP2"
			}
			pagetic = 200
		}

	} else if demosequence == 5 {
		G_DeferedPlayDemo "demo3"

	} else if demosequence == 6 {
		gamestate = GS_DEMOSCREEN
		pagename  = "CREDIT"
		pagetic   = 200

	} else if demosequence == 7 {
		G_DeferedPlayDemo "demo4"
	}

	; The Doom 3: BFG Edition version of doom2.wad does not have a
	; TITLETPIC lump, so use INTERPIC instead as a workaround.

	if gamevariant == gv_bfgedition {
		if M_StrCaseCmp pagename "TITLEPIC" == 0 {
			if W_CheckNumForName "titlepic" < 0 {
				pagename = "INTERPIC"
			}
		}
	}
}

;
; D_StartTitle
;
fun D_StartTitle () {
	gameaction   = ga_nothing
	demosequence = -1

	D_AdvanceDemo
}

#private

;
; Get game name: if the startup banner has been replaced, use that.
; Otherwise, use the name given
;
fun GetGameName (gamename ^uchar -> ^uchar) {
	return gamename
}

type PackInfo_t = struct {
	.name    ^uchar
	.mission  GameMission_t
}

const NUM_PACK_INFO = 3

rom-var pack_info [NUM_PACK_INFO]PackInfo_t = {
	{ "doom2"     mi_doom2    }
	{ "tnt"       mi_tnt      }
	{ "plutonia"  mi_plutonia }
}

fun SetMissionForPackName (pack ^uchar) {
	let i = s32 0
	loop while i < NUM_PACK_INFO {
		if M_StrCaseCmp pack [pack_info i .name] == 0 {
			gamemission = [pack_info i .mission]
			return
		}

		i = i + 1
	}

	I_Print "Valid mission packs are:\n"

	i = s32 0
	loop while i < NUM_PACK_INFO {
		I_Print2 "  %s\n" [pack_info i .name]
		i = i + 1
	}

	I_Error2 "Unknown mission pack name: %s" pack
}

;
; Find out what version of Doom is playing.
;
fun D_IdentifyVersion () {
	; gamemission is set up by the D_FindIWAD function.  But if we
	; specify '-iwad', we have to identify using IdentifyIWADByName.
	; if the iwad does not match any known IWAD name, we may have a
	; dilemma -- try to identify by its contents.

	if gamemission == mi_unknown {
		let mapkind = W_DetectMapKind

		if mapkind == 1 {
			gamemission = mi_doom
		} else if mapkind == 2 {
			gamemission = mi_doom2
		} else {
			; Still no idea.  I don't think this is going to work.
			I_Error "Unknown or invalid IWAD file."
		}

	}

	; Make sure gamemode is set up correctly

	doom1ish = matches? gamemission mi_doom mi_chex

	if doom1ish {
		; Doom 1.  But which version?
		if W_CheckNumForName "E4M1" > 0 {
			gamemode = retail  ; Ultimate Doom
		} else if W_CheckNumForName "E3M1" > 0 {
			gamemode = registered
		} else {
			gamemode = shareware
		}
	} else {
		; Doom 2 of some kind.
		gamemode = commercial

		; We can manually override the gamemission that we got from the
		; IWAD detection code. This allows us to eg. play Plutonia 2
		; with Freedoom and get the right level names.

		let p = M_CheckParmWithArgs "-pack" 1

		if p > 0 {
			let arg = M_GetArg (p + 1)
			SetMissionForPackName arg
		}
	}
}

fun D_DetermineVariant () {
	let lump = W_CheckNumForName "FREEDOOM"

	if lump >= 0 {
		lump = W_CheckNumForName "FREEDM"

		if lump >= 0 {
			gamevariant = gv_freedm
		} else {
			gamevariant = gv_freedoom
		}

		return
	}

	lump = W_CheckNumForName "DMENUPIC"

	if lump >= 0 {
		gamevariant = gv_bfgedition
	}
}

fun D_DetermineChexVariant () {
	; andrewj: detect Chex Quest 2 by looking at TITLEPIC lump

	if gamemission == mi_chex {
		let data = cast ^[0]u8 W_CacheLumpName "TITLEPIC"
		if [data 0x540] == 0x4b {
			gamevariant = gv_chex2
		}
	}
}

; These are the lumps that will be checked in IWAD,
; if any one is not present, execution will be aborted.
const NUM_REG_LUMPS = 23

rom-var reg_lump_names [NUM_REG_LUMPS]^uchar = {
	"e2m1" "e2m2" "e2m3" "e2m4" "e2m5" "e2m6" "e2m7" "e2m8" "e2m9"
	"e3m1" "e3m3" "e3m3" "e3m4" "e3m5" "e3m6" "e3m7" "e3m8" "e3m9"
	"dphoof" "bfgga0" "heada1" "cybra1" "spida1d1"
}

fun D_VerifyRegistered () {
	; okay when no -file
	if not modifiedgame {
		return
	}

	; no restrictions with FreeDoom
	if gamevariant == gv_freedoom {
		return
	}

	if gamemode == shareware {
		I_Error "\nYou cannot -file with the shareware version."
	}

	if gamemode != registered {
		return
	}

	; Check for fake IWAD with right name,
	; but w/o all the lumps of the registered version.
	let i = s32 0
	loop while i < NUM_REG_LUMPS {
		let lump = W_CheckNumForName [reg_lump_names i]

		if lump < 0 {
			I_Error "\nThis is not the registered version."
		}

		i = i + 1
	}
}

; Set the game description string

fun D_SetGameDescription () {
	let desc = ^uchar "Unknown"

	if doom1ish {
		; Doom 1.  But which version?

		if gamevariant == gv_freedoom {
			desc = GetGameName "Freedoom: Phase 1"
		} else if gamevariant == gv_chex2 {
			desc = GetGameName "Chex Quest 2"
		} else if gamemission == mi_chex {
			desc = GetGameName "Chex Quest"
		} else if gamemode == retail {
			desc = GetGameName "The Ultimate DOOM"
		} else if gamemode == registered {
			desc = GetGameName "DOOM Registered"
		} else if gamemode == shareware {
			desc = GetGameName "DOOM Shareware"
		}
	} else {
		; Doom 2 of some kind.  But which mission?

		if gamevariant == gv_freedm {
			desc = GetGameName "FreeDM"
		} else if gamevariant == gv_freedoom {
			desc = GetGameName "Freedoom: Phase 2"
		} else if gamemission == mi_doom2 {
			desc = GetGameName "DOOM 2: Hell on Earth"
		} else if gamemission == mi_plutonia {
			desc = GetGameName "DOOM 2: Plutonia Experiment"
		} else if gamemission == mi_tnt {
			desc = GetGameName "DOOM 2: TNT - Evilution"
		} else if gamemission == mi_hacx {
			desc = GetGameName "HacX 1.2"
		}
	}

	gamedescription = desc
}

fun D_SetSavegameDir () {
	; andrewj: keep savegames of different games separate

	let subdir = ^uchar "other/"

	if gamemission == mi_doom {
		subdir = "doom/"
	} else if gamemission == mi_doom2 {
		subdir = "doom2/"
	} else if gamemission == mi_tnt {
		subdir = "tnt/"
	} else if gamemission == mi_plutonia {
		subdir = "plutonia/"
	} else if gamemission == mi_chex {
		subdir = "chex/"
	} else if gamemission == mi_hacx {
		subdir = "hacx/"
	}

	let dir = M_StringJoin configdir subdir

	savegamedir = dir

	I_MakeDirectory dir
}

fun D_AddFile (filename ^uchar -> bool) {
	I_Print2 "  adding %s\n" filename

	let handle = W_AddFile filename

	return ref? handle
}

type VersionInfo_t = struct {
	.desc     ^uchar
	.cmdline  ^uchar
	.version   GameVersion_t
}

const NUM_GAME_VERSIONS = 9

rom-var version_table [NUM_GAME_VERSIONS]VersionInfo_t = {
	{ "Doom 1.666"         "1.666"      exe_doom_1_666 }
	{ "Doom 1.7/1.7a"      "1.7"        exe_doom_1_7   }
	{ "Doom 1.8"           "1.8"        exe_doom_1_8   }
	{ "Doom 1.9"           "1.9"        exe_doom_1_9   }
	{ "Hacx"               "hacx"       exe_hacx       }

	{ "Ultimate Doom"      "ultimate"   exe_ultimate   }
	{ "Final Doom"         "final"      exe_final      }
	{ "Final Doom (alt)"   "final2"     exe_final2     }
	{ "Chex Quest"         "chex"       exe_chex       }
}

; Initialize the game version

fun D_InitGameVersion () {
	;
	; Emulate a specific version of Doom.
	; Valid values are "1.666", "1.7", "1.8", "1.9",
	; "ultimate", "final", "final2", "hacx" and "chex".
	;
	let p = M_CheckParmWithArgs "-gameversion" 1

	if p > 0 {
		let arg = M_GetArg (p + 1)

		let i = s32 0
		loop while i < NUM_GAME_VERSIONS {
			if M_StrCmp [version_table i .cmdline] arg == 0 {
				gameversion = [version_table i .version]
				jump done
			}

			i = i + 1
		}

		I_Print "Supported game versions:\n"

		i = s32 0
		loop while i < NUM_GAME_VERSIONS {
			I_Print2 "  %s "  [version_table i .cmdline]
			I_Print2 "(%s)\n" [version_table i .desc]
			i = i + 1
		}

		I_Error2 "Unknown game version '%s'" arg
	} else {
		; Determine automatically

		if gamemission == mi_chex {
			; chex.exe - identified by iwad filename
			gameversion = exe_chex

		} else if gamemission == mi_hacx {
			; hacx.exe - identified by iwad filename
			gameversion = exe_hacx

		} else if gamemode == retail {
			gameversion = exe_ultimate

		} else if gamemode == commercial and gamemission != mi_doom2 {
			; Final Doom: tnt or plutonia
			; Defaults to emulating the first Final Doom executable,
			; which has the crash in the demo loop; however, having
			; this as the default should mean that it plays back
			; most demos correctly.
			gameversion = exe_final

		} else {
			if matches? gamemode shareware registered commercial {
				; original
				gameversion = exe_doom_1_9

				; detect version from demo lump
				jump done if GameVersionFromDemo 1
				jump done if GameVersionFromDemo 2
				jump done if GameVersionFromDemo 3
			}
		}
	}

	do done {}

	; the original exe does not support retail - 4th episode not supported
	if gamemode == retail {
		if gameversion < exe_ultimate {
			gamemode = registered
		}
	}

	; EXEs prior to the Final Doom one do not support Final Doom.
	if gamemode == commercial {
		let is_tnt = gamemission == mi_tnt
		let is_plut = gamemission == mi_plutonia

		if is_tnt or is_plut {
			if gameversion < exe_final {
				gamemission = mi_doom2
			}
		}
	}
}

fun GameVersionFromDemo (num s32 -> bool) {
	let lumpname = stack-var [16]uchar

	M_FormatNum lumpname 16 "demo%d" num

	if W_CheckNumForName lumpname < 0 {
		return FALSE
	}

	let demolump    = cast ^[0]u8 W_LoadLumpName lumpname PU_STATIC
	let demoversion = [demolump 0]

	Z_Free demolump

	if demoversion == 106 {
		gameversion = exe_doom_1_666
	} else if demoversion == 107 {
		gameversion = exe_doom_1_7
	} else if demoversion == 108 {
		gameversion = exe_doom_1_8
	} else if demoversion == 109 {
		gameversion = exe_doom_1_9
	} else {
		return FALSE
	}

	return TRUE
}

fun D_PrintGameVersion () {
	let i = s32 0
	loop while i < NUM_GAME_VERSIONS {
		if [version_table i .version] == gameversion {
			I_Print  "Emulating the behavior of the "
			I_Print2 "'%s' executable.\n" [version_table i .desc]
			break
		}

		i = i + 1
	}
}

fun D_LoadDemoFile (arg ^uchar, out_lump_name ^[10]uchar) {
	let file = stack-var [PATH_SIZE]uchar

	; With Vanilla you have to specify the file without extension,
	; but make that optional.
	M_StringCopy file arg PATH_SIZE

	if not (M_HasExtension file "lmp") {
		M_StringConcat file ".lmp" PATH_SIZE
	}

	if D_AddFile file {
		W_LastLumpName out_lump_name
	} else {
		; If file failed to load, still continue trying to play
		; the demo in the same way as Vanilla Doom.  This makes
		; tricks like "-playdemo demo1" possible.

		M_StringCopy out_lump_name arg 10
	}

	I_Print2 "Playing demo %s.\n" file
}

#public

;
; D_DoomMain
;
fun D_DoomMain (-> no-return) {
	D_InitDisplayState

	; print banner
	I_Print "Welcome to Doom-RZ.\n"

	Z_Init

	;
	; Disable monsters.
	;
	nomonsters = M_ParmExists "-nomonsters"

	;
	; Monsters respawn after being killed.
	;
	respawnparm = M_ParmExists "-respawn"

	;
	; Monsters move faster.
	;
	fastparm = M_ParmExists "-fast"

	;
	; Start a deathmatch game.
	;
	if M_ParmExists "-deathmatch" {
		deathmatch = 1
	}

	;
	; Start a deathmatch 2.0 game.  Weapons do not stay in place and
	; all items respawn after 30 seconds.
	;
	if M_ParmExists "-altdeath" {
		deathmatch = 2
	}

	; find which dir to use for config files
	M_SetConfigDir NULL

	;
	; Turbo mode.  The player's speed is multiplied by x%.  If unspecified,
	; x defaults to 200.  Values are rounded up to 10 and down to 400.
	;
	let p = M_CheckParm "-turbo"

	if p > 0 {
		let scale = s32 200

		p = p + 1
		if p < myargc {
			scale = M_Atoi (M_GetArg p)
		}

		scale = max scale 10
		scale = min scale 400

		G_AdjustMovementSpeed scale

		I_Print3 "turbo scale: %d%\n" scale
	}

	; init subsystems
	I_Print "V_Init: allocate screens.\n"
	V_Init

	; Load configuration files before initialising other subsystems.
	I_Print "M_LoadDefaults: Load system defaults.\n"
	D_BindVariables
	M_LoadDefaults

	; Find main IWAD file and load it.
	let iwadfile = D_FindIWAD [ref gamemission]

	; None found?
	if null? iwadfile {
		I_Error ("Game mode indeterminate.  No IWAD file was found.  Try\n" +
				 "specifying one with the '-iwad' command line parameter.")
	}

	modifiedgame = FALSE

	I_Print "W_Init: Init WADfiles.\n"
	D_AddFile iwadfile

	; Now that we've loaded the IWAD, we can figure out what gamemission
	; we're playing and which version of Vanilla Doom we need to emulate.
	D_IdentifyVersion
	D_InitGameVersion

	; Check which IWAD variant we are using.
	D_DetermineVariant

	if M_ParmExists "-deh" {
		I_Error "the -deh option is not supported"
	} else if M_ParmExists "-dehlump" {
		I_Error "the -dehlump option is not supported"
	}

	; Load PWAD files.
	modifiedgame = W_ParseCommandLine

	;; DEBUG:
	;;    W_PrintDirectory

	D_DetermineChexVariant

	; andrewj: -fastforward <MAP> feature
	p = M_CheckParmWithArgs "-fastforward" 1
	if p > 0 {
		let arg = M_GetArg (p + 1)
		fastfwd_map = M_Atoi arg
	}

	;
	; Play back the demo with given filename.
	;
	let demo_p1 = M_CheckParmWithArgs "-playdemo" 1
	let demo_p2 = M_CheckParmWithArgs "-timedemo" 1
	let demo_lump = stack-var [10]uchar

	if demo_p1 > 0 or demo_p2 > 0 {
		p = max demo_p1 demo_p2
		let arg = M_GetArg (p + 1)
		D_LoadDemoFile arg demo_lump
	}

	; Generate the WAD hash table.  Speed things up a bit.
	W_GenerateHashTable

	; Set the gamedescription string. This is only possible now that
	; we've finished loading Dehacked patches.
	D_SetGameDescription

	D_SetSavegameDir

	; Check for -file in shareware
	D_VerifyRegistered

	let ss_start = W_CheckNumForName "SS_START"
	let ff_end   = W_CheckNumForName "FF_END"

	if max ss_start ff_end >= 0 {
		I_PrintDivider
		I_Print " WARNING: The loaded WAD file contains modified sprites or\n"
		I_Print " floor textures.  You may want to use the '-merge' command\n"
		I_Print " line option instead of '-file'.\n"
	}

	I_PrintStartupBanner gamedescription

	I_Print "I_Init: Setting up machine state.\n"
	I_InitTimer
	I_InitSound
	I_InitMusic

	let genmidi = W_LoadLumpName "genmidi" PU_STATIC
	I_OPL_SetGENMIDI genmidi

	D_ConnectNetGame

	; get skill / episode / map from parms

	autostart    = FALSE
	startskill   = sk_medium
	startepisode = 1
	startmap     = 1

	;
	; Set the game skill, 1-5 (1: easiest, 5: hardest).  A skill of
	; 0 disables all monsters.
	;
	p = M_CheckParmWithArgs "-skill" 1

	if p > 0 {
		let arg = M_GetArg (p + 1)
		let num = M_Atoi arg
		startskill = num - 1
		autostart  = TRUE
	}

	;
	; Start playing on episode n (1-4)
	;
	p = M_CheckParmWithArgs "-episode" 1

	if p > 0 {
		let arg = M_GetArg (p + 1)
		startepisode = M_Atoi arg
		startmap     = 1
		autostart    = TRUE
	}

	;
	; Start a game immediately, warping to ExMy (Doom 1) or MAPxy
	; (Doom 2)
	;
	p  = M_CheckParmWithArgs "-warp" 1
	let p2 = M_CheckParmWithArgs "-warp" 2

	if p > 0 {
		let arg = M_GetArg (p + 1)

		if gamemode == commercial {
			startmap = M_Atoi arg
		} else {
			startepisode = M_Atoi arg

			if p2 > 0 {
				arg = M_GetArg (p + 2)
				startmap = M_Atoi arg
			}
		}

		autostart = TRUE
	}

	; Check for load game parameter.
	; We do this here and save the slot number, so that the network code
	; can override it or send the load slot to other players.

	p = M_CheckParmWithArgs "-loadgame" 1

	if p > 0 {
		let arg = M_GetArg (p + 1)
		startloadgame = M_Atoi arg
	} else {
		; Not loading a game
		startloadgame = -1
	}

	I_Print "M_Init: Init miscellaneous info.\n"
	M_Init

	I_Print "R_Init: Init DOOM refresh daemon - "
	R_Init

	I_Print "\nP_Init: Init Playloop state.\n"
	P_Init

	I_Print "S_Init: Setting up sound.\n"
	S_Init (sfxVolume * 8) (musicVolume * 8)

	if gamemission == mi_hacx {
		S_InitHacx
	}

	I_Print "D_CheckNetGame: Checking network game status.\n"
	D_CheckNetGame
	D_PrintGameVersion

	I_Print "HU_Init: Setting up heads up display.\n"
	HU_Init

	I_Print "ST_Init: Init status bar.\n"
	ST_Init

	;
	; Record a demo named x.lmp.
	;
	p = M_CheckParmWithArgs "-record" 1

	if p > 0 {
		let arg = M_GetArg (p + 1)
		G_RecordDemo arg
		autostart = TRUE
	}

	if demo_p1 > 0 {
		singledemo = TRUE  ; quit after one demo
		G_DeferedPlayDemo demo_lump
		jump run_game
	}

	if demo_p2 > 0 {
		G_TimeDemo demo_lump
		jump run_game
	}

	if startloadgame >= 0 {
		G_LoadGame startloadgame
	}

	if gameaction != ga_loadgame {
		if autostart or netgame {
			G_InitNew startskill startepisode startmap
		} else {
			; start up intro loop
			D_StartTitle
		}
	}

	do run_game {
		; this never returns
		D_DoomLoop
	}
}

;
; D_ShutdownEngine
;
fun D_ShutdownEngine () {
	M_SaveDefaults
	G_CheckDemoFinished FALSE
	;; D_Endoom
}
