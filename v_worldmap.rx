;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

; parms for world map / intermission
zero-var wminfo wbstartstruct_t


#private

const NEXTLOCDELAY = 4 * TICRATE

; global locations
const WI_TITLEY    = 2

; single-player layout
const SP_STATSX1   = 50
const SP_STATSX2   = 270
const SP_STATSY    = 50

const SP_TIMEX1    = 16
const SP_TIMEX2    = 144
const SP_TIMEY     = SCREENHEIGHT - 32

const SP_PARX1     = 176
const SP_PARX2     = 304
const SP_PARY      = SCREENHEIGHT - 32

; net-game layout
const NG_STATSY    = 50
const NG_STATSX    = 28
const NG_SPACINGX  = 64
const NG_SPACINGY  = 34

; deathmatch layout
const DM_MATRIXX   = 42
const DM_MATRIXY   = 68

const DM_SPACINGX  = 40
const DM_SPACINGY  = 33
const DM_TOTALSX   = 269

const DM_KILLERSX  = 10
const DM_KILLERSY  = 100
const DM_VICTIMSX  = 5
const DM_VICTIMSY  = 50

;
; Animation.
;
type wi_animtype_e = s32

const ANIM_END    = 0
const ANIM_ALWAYS = 1
const ANIM_RANDOM = 2
const ANIM_LEVEL  = 3

type wi_point_t = struct {
	.x  s32
	.y  s32
}

type wi_anim_t = struct {
	.type      wi_animtype_e

	; period in tics between animations
	.period    s32

	; number of animation frames
	.frames    s32

	; location of animation
	.loc       wi_point_t

	; for ANIM_RANDOM: period deviation (< 256)
	; for ANIM_LEVEL:  level
	.data1     s32

	; -- following must be initialized to zero before use! --

	; time when animation will change
	.nexttic   s32

	; next frame number in animation
	.ctr       s32

	; used by ANIM_RANDOM and ANIM_LEVEL when animating
	.state     s32
}

rom-var locnodes [3][9]wi_point_t = {
	; Episode 1 World Map
	{
		{ 185 164 }
		{ 148 143 }
		{  69 122 }
		{ 209 102 }
		{ 116  89 }
		{ 166  55 }
		{  71  56 }
		{ 135  29 }
		{  71  24 }
	}

	; Episode 2 World Map
	{
		{ 254  25 }
		{  97  50 }
		{ 188  64 }
		{ 128  78 }
		{ 214  92 }
		{ 133 130 }
		{ 208 136 }
		{ 148 140 }
		{ 235 158 }
	}

	; Episode 3 World Map
	{
		{ 156 168 }
		{  48 154 }
		{ 174  95 }
		{ 265  75 }
		{ 130  48 }
		{ 279  23 }
		{ 198  48 }
		{ 140  25 }
		{ 281 136 }
	}
}

;
; Animation locations for episodes 1..3
; Using patches saves a lot of space,
; as they replace 320x200 full screen frames.
;
var wi_anims [3][11]wi_anim_t = {
	; Episode 1 World Map
	{
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 224 104 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 184 160 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 112 136 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 {  72 112 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 {  88  96 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 {  64  48 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 192  40 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 136  16 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 {  80  16 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 {  64  24 } ... }
		...
	}

	; Episode 2 World Map
	{
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 1 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 2 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 3 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 4 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 5 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 6 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 7 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 3 { 192 144 } 8 ... }
		{ ANIM_LEVEL  (TICRATE / 3) 1 { 128 136 } 8 ... }
		...
	}

	; Episode 2 World Map
	{
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 104 168 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 {  40 136 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 160  96 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 104  80 } ... }
		{ ANIM_ALWAYS (TICRATE / 3) 3 { 120  32 } ... }
		{ ANIM_ALWAYS (TICRATE / 4) 3 {  40   0 } ... }
		...
	}
}

;;
;; GENERAL DATA
;;

type wi_state_e = s32

const WIS_StatCount  = 0  ; displaying the kills/items/secrets/etc
const WIS_NextLoc    = 1  ; displaying a flashing pointer (Doom I only)
const WIS_Transition = 2  ; brief pause until start next level

type WorldIntermissionState = struct {
	.state      wi_state_e

	.tics       s32   ; total number of tics since start
	.count      s32   ; used for general timing
	.line       s32   ; which line being done in a stage, or'd with 1 for pause
	.pause      s32   ; used for the pause between each part

	.kills      [MAXPLAYERS]s32  ; percentages
	.items      [MAXPLAYERS]s32  ;
	.secret     [MAXPLAYERS]s32  ;
	.frags      [MAXPLAYERS]s32  ;

	.cnt_kills    [MAXPLAYERS]s32  ; counting up values
	.cnt_items    [MAXPLAYERS]s32  ;
	.cnt_secret   [MAXPLAYERS]s32  ;
	.cnt_frags    [MAXPLAYERS]s32  ;

	.dm_frags   [MAXPLAYERS][MAXPLAYERS]s32

	.time  s32  ; converted to seconds
	.par   s32  ;

	.cnt_time  s32  ; counting up values
	.cnt_par   s32  ;

	.accel      bool  ; player wants to accelerate or skip a stage
	.pointer    bool  ; used to flash the "you are here" pointer
}

zero-var wi WorldIntermissionState

;;
;; CODE
;;

fun WI_slamBackground () {
	if gamemode == commercial {
		V_DrawPatchName 0 0 "INTERPIC"
		return
	}

	if [wminfo .episode] >= 4 {
		V_DrawPatchName 0 0 "INTERPIC"
		return
	}

	let epi-1 = [wminfo .episode] - 1
	let picname = stack-var [16]uchar

	M_FormatNum picname 16 "WIMAP%d" epi-1
	V_DrawPatchName 0 0 picname
}

fun WI_drawLevelFinished () {
	let y = s32 WI_TITLEY

	let picname = stack-var [16]uchar
	WI_patchForLevelName picname FALSE

	if [picname 0] == 0 {
		return
	}

	; draw level name
	let name_p = cast ^patch_t W_CacheLumpName picname

	let x = s32 SCREENWIDTH - V_PatchWidth name_p
	x = x / 2

	V_DrawPatch x y name_p

	let step = V_PatchHeight name_p
	step = step * 5 / 4
	y    = y + step

	; draw "FINISHED"
	let finish_p = cast ^patch_t W_CacheLumpName "WIF"

	x = s32 SCREENWIDTH - V_PatchWidth finish_p
	x = x / 2

	V_DrawPatch x y finish_p
}

fun WI_drawEnteringLevel () {
	let y = s32 WI_TITLEY

	let picname = stack-var [16]uchar
	WI_patchForLevelName picname TRUE

	if [picname 0] == 0 {
		return
	}

	; draw "ENTERING"
	let enter_p = cast ^patch_t W_CacheLumpName "WIF"

	let x = (s32 SCREENWIDTH - V_PatchWidth enter_p) / 2

	V_DrawPatch x y enter_p

	let step = V_PatchHeight enter_p
	step = step * 5 / 4
	y    = y + step

	; draw level name
	let name_p = cast ^patch_t W_CacheLumpName picname

	x = (s32 SCREENWIDTH - V_PatchWidth name_p) / 2

	V_DrawPatch x y name_p
}

fun WI_patchForLevelName (picname ^[16]uchar, next bool) {
	[picname 0] = 0

	let epi-1 = [wminfo .episode] - 1
	let map-1 = [wminfo .last] - 1

	if next {
		map-1 = [wminfo .next] - 1
	}

	if gamemode == commercial {
		if map-1 < 0 {
			return
		} else if map-1 < 10 {
			M_FormatNum picname 16 "CWILV0%d" map-1
		} else if map-1 < 31 {
			M_FormatNum picname 16 "CWILV%d" map-1
		}

	} else {
		if epi-1 < 0 {
			return
		} else if epi-1 > 3 {
			return
		} else if map-1 < 0 {
			return
		}

		M_FormatNum picname 16 "WILV%d0" epi-1
		[picname 5] = '0' + (uchar map-1)
	}
}

fun WI_drawLocNode (map s32, ptr bool) {
	let epi-1 = [wminfo .episode] - 1
	let map-1 = map - 1

	let x = [locnodes epi-1 map-1 .x]
	let y = [locnodes epi-1 map-1 .y]

	; andrewj: simplified logic here, don't test for fitting.
	; this is safe since V_DrawPatch now clips to the screen.

	if ptr {
		if x < (SCREENWIDTH / 2) {
			V_DrawPatchName x y "WIURH0"
		} else {
			V_DrawPatchName x y "WIURH1"
		}
	} else {
		V_DrawPatchName x y "WISPLAT"
	}
}

fun WI_initAnimations () {
	if gamemode == commercial {
		return
	} else if [wminfo .episode] >= 4 {
		return
	}

	let epi-1 = [wminfo .episode] - 1

	let i = s32 0
	loop {
		let a = [ref wi_anims epi-1 i]
		break if [a .type] == ANIM_END

		; init variables
		[a .ctr] = -1

		let r = M_Random

		; specify the next time to draw it
		if [a .type] == ANIM_ALWAYS {
			r = r % [a .period]
		} else if [a .type] == ANIM_RANDOM {
			r = r % [a .data1]
		} else if [a .type] == ANIM_LEVEL {
			r = 0
		}

		[a .nexttic] = [wi .tics] + 1 + r

		i = i + 1
	}
}

fun WI_updateAnimatedBack () {
	if gamemode == commercial {
		return
	} else if [wminfo .episode] >= 4 {
		return
	}

	let epi-1 = [wminfo .episode] - 1

	let i = s32 0
	loop {
		let a = [ref wi_anims epi-1 i]
		break if [a .type] == ANIM_END

		; time to change?
		if [a .nexttic] == [wi .tics] {
			if [a .type] == ANIM_ALWAYS {
				[a .ctr] = [a .ctr] + 1

				if [a .ctr] >= [a .frames] {
					[a .ctr] = 0
				}

				[a .nexttic] = [wi .tics] + [a .period]

			} else if [a .type] == ANIM_RANDOM {
				; andrewj: not used by anything, so not ported

			} else if [a .type] == ANIM_LEVEL {
				; andrewj: I couldn't understand what this did, no big loss
			}
		}

		i = i + 1
	}
}

fun WI_drawAnimatedBack () {
	WI_slamBackground

	if gamemode == commercial {
		return
	} else if [wminfo .episode] >= 4 {
		return
	} else if [wminfo .episode] == 2 {
		; andrewj: second episode is weird, just do nothing for now
		return
	}

	let epi-1 = [wminfo .episode] - 1

	let i = s32 0
	loop {
		let a = [ref wi_anims epi-1 i]
		break if [a .type] == ANIM_END

		if [a .ctr] >= 0 {
			let picname = stack-var [16]uchar
			M_FormatNum picname 16 "WIA%d0000" epi-1

			[picname 5] = uchar ('0' + i)
			[picname 7] = uchar ('0' + [a .ctr])

			V_DrawPatchName [a .loc .x] [a .loc .y] picname
		}

		i = i + 1
	}
}

;
; Draws a number.
;
; If width > 0, add zero digits to the front to make the result
; occupy that width, unless input number is zero itself.
; Returns the new x position.
;
fun WI_drawNum (x s32, y s32, num s32, width s32 -> s32) {
	let fontwidth = s32 little-endian [[wm_font .nums 0] .width]

	let is_neg = num < 0
	num    = abs num

	if width < 0 {
		; figure out # of digits
		width = 0
		let temp  = num

		loop while temp != 0 {
			width = width + 1
			temp  = temp  / 10
		}

		width = max width 1
	}

	; draw the new number
	loop until width == 0 {
		let digit = num % 10
		num   = num / 10
		width = width - 1

		x = x - fontwidth
		V_DrawPatch x y [wm_font .nums digit]
	}

	; draw a minus sign if necessary
	if is_neg {
		x = x - 8
		V_DrawPatch x y [wm_font .minus]
	}

	return x
}

fun WI_drawPercent (x s32, y s32, num s32) {
	if num >= 0 {
		V_DrawPatch x y [wm_font .percent]
		WI_drawNum  x y num -1
	}
}

;
; Display level completion time, or "sucks" message if overflow.
;
fun WI_drawTime (x s32, y s32, time s32) {
	if time < 0 {
		return
	}

	let mins = time / 60
	time = time % 60

	if mins >= 60 {
		let sucks = cast ^patch_t W_CacheLumpName "WISUCKS"
		let w = s32 little-endian [sucks .width]
		x = x - w
		V_DrawPatch x y sucks
		return
	}

	; seconds
	x = WI_drawNum x y time 2

	; colon
	let w = s32 little-endian [[wm_font .colon] .width]
	x = x - w
	V_DrawPatch x y [wm_font .colon]

	; minutes
	if mins > 0 {
		WI_drawNum x y mins 2
	}
}

fun WI_checkForAccelerate () {
	; check for button presses to skip delays.

	let i = s32 0
	loop while i < MAXPLAYERS {
		let p = [ref players i]

		if [p .in_game] {
			let buttons  = [p .cmd .buttons]
			let b_attack = (buttons & BT_ATTACK != 0)
			let b_use    = (buttons & BT_USE    != 0)

			if b_attack {
				if not [p .attackdown] {
					[p .attackdown] = TRUE
					[wi .accel]     = TRUE
				}
			} else {
				[p .attackdown] = FALSE
			}

			if b_use {
				if not [p .usedown] {
					[p .usedown] = TRUE
					[wi .accel]  = TRUE
				}
			} else {
				[p .usedown] = FALSE
			}
		}

		i = i + 1
	}
}

fun WI_computePercent (num s32, total s32 -> s32) {
	; prevent division by zero
	total = max total 1

	num = num * 100 / total
	num = min num 999

	return num
}

fun WI_fragSum (pnum s32 -> s32) {
	let frags = s32 0

	let i = s32 0
	loop while i < MAXPLAYERS {
		let p = [ref players i]

		if [p .in_game] {
			let cur_num = [wminfo .plyr pnum .frags i]

			if i == pnum {
				frags = frags - cur_num
			} else {
				frags = frags + cur_num
			}
		}

		i = i + 1
	}

	frags = max frags -999
	frags = min frags  999

	return frags
}

;------------------------------------------------------------------------

fun WI_initTransition () {
	[wi .state] = WIS_Transition
	[wi .accel] = FALSE
	[wi .count] = 10
}

fun WI_updateTransition () {
	[wi .count]   = [wi .count] - 1
	[wi .pointer] = TRUE

	if [wi .count] <= 0 {
		G_WorldDone
	}
}

fun WI_initNextLoc () {
	if gamemode == commercial {
		WI_initTransition
		return
	}

	[wi .state] = WIS_NextLoc
	[wi .accel] = FALSE
	[wi .count] = NEXTLOCDELAY

	;; WI_initAnimations
}

fun WI_updateNextLoc () {
	[wi .count] = [wi .count] - 1

	if [wi .accel] {
		WI_initTransition
	} else if [wi .count] == 0 {
		WI_initTransition
	} else {
		let flash = [wi .count] & 31
		[wi .pointer] = flash < 20
	}
}

fun WI_drawNextLoc () {
	WI_drawAnimatedBack

	if gamemode == commercial {
		if [wminfo .last] != 30 {
			WI_drawEnteringLevel
		}

		return
	}

	if [wminfo .episode] >= 4 {
		WI_drawEnteringLevel
		return
	}

	let last = [wminfo .last]

	if last == 9 {
		; current is secret level, determine last one
		last = [wminfo .next] - 1
	}

	; draw a splat on taken cities...
	loop while last > 0 {
		WI_drawLocNode last FALSE
		last = last - 1
	}

	; splat the secret level?
	if [wminfo .did_secret] != 0 {
		WI_drawLocNode 9 FALSE
	}

	; draw flashing pointer
	if [wi .pointer] {
		WI_drawLocNode [wminfo .next] TRUE
	}

		; draws which level you are entering...
	WI_drawEnteringLevel
}

;------------------------------------------------------------------------

fun WI_initDeathmatchStats () {
	[wi .state] = WIS_StatCount
	[wi .accel] = FALSE
	[wi .line]  = 1
	[wi .pause] = TICRATE

	let i = s32 0
	loop while i < MAXPLAYERS {
		if [players i .in_game] {
			let k = s32 0
			loop while k < MAXPLAYERS {
				if [players k .in_game] {
					[wi .dm_frags i k] = 0

					; clamp result to prevent glitches
					[wminfo .plyr i .frags k] = max [wminfo .plyr i .frags k] -99
					[wminfo .plyr i .frags k] = min [wminfo .plyr i .frags k]  99
				}
				k = k + 1
			}

			[wi .cnt_frags i] = 0
			[wi .frags     i] = WI_fragSum i
		}

		i = i + 1
	}

	WI_initAnimations
}

fun WI_updateDeathmatchStats () {
	; skip to the end?
	if [wi .line] < 4 {
		if [wi .accel] {
			[wi .accel] = FALSE
			[wi .line]  = 4

			let i = s32 0
			loop while i < MAXPLAYERS {
				let k = s32 0
				loop while k < MAXPLAYERS {
					[wi .dm_frags i k] = [wminfo .plyr i .frags k]
					k = k + 1
				}
				[wi .cnt_frags i] = [wi .frags i]
				i = i + 1
			}

			S_StartSound NULL sfx_barexp
		}
	}

	; a short pause
	if [wi .line] & 1 != 0 {
		[wi .pause] = [wi .pause] - 1
		if [wi .pause] == 0 {
			[wi .line] = [wi .line] + 1
			[wi .pause] = TICRATE
		}

		return
	}

	let t = [wi .tics] & 3
	let stillticking = FALSE

	if [wi .line] == 2 {
		; andrewj: slow this down
		if t & 1 != 0 {
			return
		}

		let i = s32 0
		loop while i < MAXPLAYERS {
			if [players i .in_game] {
				; ramp up/down the row of frag values
				let k = s32 0
				loop while k < MAXPLAYERS {
					if [players k .in_game] {
						if [wi .dm_frags i k] < [wminfo .plyr i .frags k] {
							[wi .dm_frags i k] = [wi .dm_frags i k] + 1
							stillticking = TRUE
						} else if [wi .dm_frags i k] > [wminfo .plyr i .frags k] {
							[wi .dm_frags i k] = [wi .dm_frags i k] - 1
							stillticking = TRUE
						}
					}
					k = k + 1
				}

				; ramp up/down the total
				if [wi .cnt_frags i] < [wi .frags i] {
					[wi .cnt_frags i] = [wi .cnt_frags i] + 1
					stillticking = TRUE
				} else if [wi .cnt_frags i] > [wi .frags i] {
					[wi .cnt_frags i] = [wi .cnt_frags i] - 1
					stillticking = TRUE
				}
			}

			i = i + 1
		}

		if not stillticking {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; all done, waiting for a button press
	if [wi .accel] {
		WI_initNextLoc
		S_StartSound NULL sfx_slop
	}
}

fun WI_drawDeathmatchStats () {
	WI_drawAnimatedBack
	WI_drawLevelFinished

	; draw stat titles (top line)
	let x = s32 (DM_TOTALSX - 35)
	let y = s32 (DM_MATRIXY - DM_SPACINGY + 10)

	V_DrawPatchName x y "WIMSTT"

	V_DrawPatchName DM_KILLERSX DM_KILLERSY "WIKILRS"
	V_DrawPatchName DM_VICTIMSX DM_VICTIMSY "WIVCTMS"

	; draw backgrounds.
	; the console player gets a face, the rest are blank
	x = s32 (DM_MATRIXX + DM_SPACINGX - 20)
	y = s32 DM_MATRIXY

	let picname = stack-var [16]uchar

	let i = s32 0
	loop while i < MAXPLAYERS {
		if [players i .in_game] {
			M_FormatNum picname 16 "STPB%d" i

			V_DrawPatchName x (DM_MATRIXY - DM_SPACINGY) picname
			V_DrawPatchName (DM_MATRIXX - 20) y picname

			if i == [wminfo .pnum] {
				V_DrawPatchName x (DM_MATRIXY - DM_SPACINGY) "STFDEAD0"
				V_DrawPatchName (DM_MATRIXX - 20) y "STFST01"
			}
		}

		x = x + DM_SPACINGX
		y = y + DM_SPACINGY
		i = i + 1
	}

	; draw stats
	y = DM_MATRIXY + 10

	i = s32 0
	loop while i < MAXPLAYERS {
		x = DM_MATRIXX + DM_SPACINGX + 2

		if [players i .in_game] {
			let k = s32 0
			loop while k < MAXPLAYERS {
				if [players k .in_game] {
					WI_drawNum (x + 8) y [wi .dm_frags i k] 2
				}

				x = x + DM_SPACINGX
				k = k + 1
			}

			WI_drawNum (DM_TOTALSX + 8) y [wi .cnt_frags i] 2
		}

		y = y + DM_SPACINGY
		i = i + 1
	}
}

;------------------------------------------------------------------------

fun WI_initNetgameStats () {
	[wi .state] = WIS_StatCount
	[wi .accel] = FALSE
	[wi .line]  = 1
	[wi .pause] = TICRATE

	let i = s32 0
	loop while i < MAXPLAYERS {
		let p = [ref players i]

		if [p .in_game] {
			[wi .cnt_kills  i] = 0
			[wi .cnt_items  i] = 0
			[wi .cnt_secret i] = 0
			[wi .cnt_frags  i] = 0

			[wi .kills  i] = WI_computePercent [wminfo .plyr i .kills]  [wminfo .max_kills]
			[wi .items  i] = WI_computePercent [wminfo .plyr i .items]  [wminfo .max_items]
			[wi .secret i] = WI_computePercent [wminfo .plyr i .secret] [wminfo .max_secret]
			[wi .frags  i] = WI_fragSum i
		}

		i = i + 1
	}

	WI_initAnimations
}

fun WI_updateNetgameStats () {
	; skip to the end?
	if [wi .line] < 8 {
		if [wi .accel] {
			[wi .accel] = FALSE
			[wi .line]  = 8

			let i = s32 0
			loop while i < MAXPLAYERS {
				[wi .cnt_kills  i] = [wi .kills  i]
				[wi .cnt_items  i] = [wi .items  i]
				[wi .cnt_secret i] = [wi .secret i]
				[wi .cnt_frags  i] = [wi .frags  i]

				i = i + 1
			}

			S_StartSound NULL sfx_barexp
		}
	}

	; a short pause
	if [wi .line] & 1 != 0 {
		[wi .pause] = [wi .pause] - 1
		if [wi .pause] == 0 {
			[wi .line]  = [wi .line] + 1
			[wi .pause] = TICRATE
		}

		return
	}

	let t = [wi .tics] & 3
	let stillticking = FALSE

	; kills
	if [wi .line] == 2 {
		let i = s32 0
		loop while i < MAXPLAYERS {
			if [players i .in_game] {
				[wi .cnt_kills i] = [wi .cnt_kills i] + 2
				[wi .cnt_kills i] = min [wi .cnt_kills i] [wi .kills i]

				if [wi .cnt_kills i] < [wi .kills i] {
					stillticking = TRUE
				}
			}

			i = i + 1
		}

		if not stillticking {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; items
	if [wi .line] == 4 {
		let i = s32 0
		loop while i < MAXPLAYERS {
			if [players i .in_game] {
				[wi .cnt_items i] = [wi .cnt_items i] + 2
				[wi .cnt_items i] = min [wi .cnt_items i] [wi .items i]

				if [wi .cnt_items i] < [wi .items i] {
					stillticking = TRUE
				}
			}

			i = i + 1
		}

		if not stillticking {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; secrets
	if [wi .line] == 6 {
		let i = s32 0
		loop while i < MAXPLAYERS {
			if [players i .in_game] {
				[wi .cnt_secret i] = [wi .cnt_secret i] + 2
				[wi .cnt_secret i] = min [wi .cnt_secret i] [wi .secret i]

				if [wi .cnt_secret i] < [wi .secret i] {
					stillticking = TRUE
				}
			}

			i = i + 1
		}

		if not stillticking {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; andrewj: disabled the display of frags

	; all done, waiting for a button press
	if [wi .accel] {
		WI_initNextLoc
		S_StartSound NULL sfx_sgcock
	}
}

fun WI_drawNetgameStats () {
	WI_drawAnimatedBack
	WI_drawLevelFinished

	let line_h = s32 little-endian [[wm_font .nums 0] .height]
	line_h = line_h * 3 / 2

	; draw stat titles (top line)
	let x = s32 NG_STATSX
	let y = s32 NG_STATSY

	x = x + 56
	V_DrawPatchName x y "WIOSTK"

	x = x + NG_SPACINGX
	V_DrawPatchName x y "WIOSTI"

	x = x + NG_SPACINGX + 2
	V_DrawPatchName x y "WIOSTS"

	;; V_DrawPatchName (NG_STATSX+4*NG_SPACINGX-SHORT(frags->width), NG_STATSY "WIFRGS"

	let picname = stack-var [16]uchar

	; draw stats
	y = y + line_h

	let i = s32 0
	loop while i < MAXPLAYERS {
		if [players i .in_game] {
			; face background for each player
			x = s32 NG_STATSX + 12
			M_FormatNum picname 16 "STPB%d" i
			V_DrawPatchName x y picname

			; show a face for the console player only
			if i == [wminfo .pnum] {
				V_DrawPatchName x y "STFST01"
			}

			y = y + 5
			x = x + 20 + NG_SPACINGX
			WI_drawPercent x y [wi .cnt_kills i]

			x = x + NG_SPACINGX
			WI_drawPercent x y [wi .cnt_items i]

			x = x + NG_SPACINGX
			WI_drawPercent x y [wi .cnt_secret i]

			; andrewj: disabled the display of frags

			;;  x = iadd x NG_SPACINGX
			;;	WI_drawNum x y [wi .cnt_frags i] -1

			y = y + NG_SPACINGY + -5
		}

		i = i + 1
	}
}

;------------------------------------------------------------------------

fun WI_initSingleStats () {
	[wi .state] = WIS_StatCount
	[wi .accel] = FALSE
	[wi .line]  = 1

	; default is -1 to prevent showing anything while earlier parts
	; of the stats are still counting up.

	[wi .cnt_kills  0] = -1
	[wi .cnt_items  0] = -1
	[wi .cnt_secret 0] = -1

	[wi .cnt_time] = -1
	[wi .cnt_par]  = -1

	let me = [wminfo .pnum]

	[wi .kills  0] = WI_computePercent [wminfo .plyr me .kills]  [wminfo .max_kills]
	[wi .items  0] = WI_computePercent [wminfo .plyr me .items]  [wminfo .max_items]
	[wi .secret 0] = WI_computePercent [wminfo .plyr me .secret] [wminfo .max_secret]

	[wi .time] = [wminfo .plyr me .time] / TICRATE
	[wi .par]  = [wminfo .partime] / TICRATE

	[wi .pause] = TICRATE

	WI_initAnimations
}

fun WI_updateSingleStats () {
	; skip to the end?
	if [wi .line] < 10 {
		if [wi .accel] {
			[wi .accel] = FALSE
			[wi .line]  = 10

			[wi .cnt_kills  0] = [wi .kills  0]
			[wi .cnt_items  0] = [wi .items  0]
			[wi .cnt_secret 0] = [wi .secret 0]
			[wi .cnt_time]     = [wi .time]
			[wi .cnt_par]      = [wi .par]

			S_StartSound NULL sfx_barexp
			return
		}
	}

	; a short pause
	if [wi .line] & 1 != 0 {
		[wi .pause] = [wi .pause] - 1
		if [wi .pause] == 0 {
			[wi .line]  = [wi .line] + 1
			[wi .pause] = TICRATE
		}

		return
	}

	let t = [wi .tics] & 3

	; kills
	if [wi .line] == 2 {
		[wi .cnt_kills 0] = [wi .cnt_kills 0] + 2
		[wi .cnt_kills 0] = min [wi .cnt_kills 0] [wi .kills 0]

		if [wi .cnt_kills 0] == [wi .kills 0] {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; items
	if [wi .line] == 4 {
		[wi .cnt_items 0] = [wi .cnt_items 0] + 2
		[wi .cnt_items 0] = min [wi .cnt_items 0] [wi .items 0]

		if [wi .cnt_items 0] == [wi .items 0] {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; secrets
	if [wi .line] == 6 {
		[wi .cnt_secret 0] = [wi .cnt_secret 0] + 2
		[wi .cnt_secret 0] = min [wi .cnt_secret 0] [wi .secret 0]

		if [wi .cnt_secret 0] == [wi .secret 0] {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; time
	if [wi .line] == 8 {
		[wi .cnt_time] = [wi .cnt_time] + 3
		[wi .cnt_par]  = [wi .cnt_par]  + 3

		[wi .cnt_time] = min [wi .cnt_time] [wi .time]
		[wi .cnt_par] = min [wi .cnt_par] [wi .par]

		let reached1 = [wi .cnt_time] == [wi .time]
		let reached2 = [wi .cnt_par]  == [wi .par]

		if reached1 and reached2 {
			[wi .line] = [wi .line] + 1
			S_StartSound NULL sfx_barexp
		} else if t == 0 {
			S_StartSound NULL sfx_pistol
		}

		return
	}

	; all done, waiting for a button press
	if [wi .accel] {
		WI_initNextLoc
		S_StartSound NULL sfx_sgcock
	}
}

fun WI_drawSingleStats () {
	WI_drawAnimatedBack
	WI_drawLevelFinished

	let line_h = s32 little-endian [[wm_font .nums 0] .height]
	line_h = line_h * 3 / 2

	; kills
	let y = s32 SP_STATSY
	V_DrawPatchName SP_STATSX1 y "WIOSTK"
	WI_drawPercent  SP_STATSX2 y [wi .cnt_kills 0]

	; items
	y = y + line_h
	V_DrawPatchName SP_STATSX1 y "WIOSTI"
	WI_drawPercent  SP_STATSX2 y [wi .cnt_items 0]

	; secret
	y = y + line_h
	V_DrawPatchName SP_STATSX1 y "WISCRT2"
	WI_drawPercent  SP_STATSX2 y [wi .cnt_secret 0]

	; time
	V_DrawPatchName SP_TIMEX1 SP_TIMEY "WITIME"
	WI_drawTime     SP_TIMEX2 SP_TIMEY [wi .cnt_time]

	; par
	if [wminfo .partime] > 0 {
		V_DrawPatchName SP_PARX1 SP_PARY "WIPAR"
		WI_drawTime     SP_PARX2 SP_PARY [wi .cnt_par]
	}
}

fun WI_initVariables () {
	[wi .tics]  = 0
	[wi .count] = 0
	[wi .accel] = FALSE
}

;------------------------------------------------------------------------

#public

fun WI_Start () {
	WI_initVariables

	if deathmatch != 0 {
		WI_initDeathmatchStats
	} else if netgame {
		WI_initNetgameStats
	} else {
		WI_initSingleStats
	}
}

fun WI_Drawer () {
	if [wi .state] == WIS_StatCount {
		if deathmatch != 0 {
			WI_drawDeathmatchStats
		} else if netgame {
			WI_drawNetgameStats
		} else {
			WI_drawSingleStats
		}
	} else {
		WI_drawNextLoc
	}
}

; NOTE: there is no WI_Responder.  The ticker is used to
;       detect keys (because of timing issues in netgames).

fun WI_Ticker () {
	; counter for general background animation
	[wi .tics] = [wi .tics] + 1

	; start intermission music
	if [wi .tics] == 1 {
		if gamemode == commercial {
			S_ChangeMusic mus_dm2int TRUE
		} else {
			S_ChangeMusic mus_inter  TRUE
		}
	}

	WI_checkForAccelerate

	WI_updateAnimatedBack

	if [wi .state] == WIS_StatCount {
		if deathmatch != 0 {
			WI_updateDeathmatchStats
		} else if netgame {
			WI_updateNetgameStats
		} else {
			WI_updateSingleStats
		}

	} else if [wi .state] == WIS_NextLoc {
		WI_updateNextLoc

	} else {
		WI_updateTransition
	}
}
