;;
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

;
; Ideally this should work exactly the same as in deutex, but trying to
; read the deutex source code made my brain hurt.
;

#private

type section_t = s32

const SECTION_NORMAL  = 0
const SECTION_FLATS   = 1
const SECTION_SPRITES = 2

type searchlist_t = struct {
	.lumps ^[0]^lumpinfo_t
	.count  s32
}

type sprite_frame_t = struct {
	.sprite  [4]uchar
	.frame   uchar
	.angles  [8]^lumpinfo_t
}

zero-var sl_iwad          searchlist_t
zero-var sl_iwad_sprites  searchlist_t
zero-var sl_iwad_flats    searchlist_t

zero-var sl_pwad          searchlist_t
zero-var sl_pwad_sprites  searchlist_t
zero-var sl_pwad_flats    searchlist_t

; lumps with these sprites must be replaced in the IWAD
zero-var sprite_frames  ^[0]sprite_frame_t
zero-var sprite_frames_num    u32
zero-var sprite_frames_total  u32

fun NewSpriteFrame (-> ^sprite_frame_t) {
	; grow list?
	if sprite_frames_num >= sprite_frames_total {
		if sprite_frames_total == 0 {
			sprite_frames_total = 128
			let size = sprite_frames_total * sprite_frame_t.size
			sprite_frames = I_Alloc size
		} else {
			sprite_frames_total = sprite_frames_total * 2
			let size = sprite_frames_total * sprite_frame_t.size
			sprite_frames = I_Realloc sprite_frames size
		}
	}

	let result = [ref sprite_frames sprite_frames_num]
	sprite_frames_num = sprite_frames_num + 1

	return result
}

; Search in a list to find a lump with a particular name
; Linear search (slow!)
;
; Returns -1 if not found

fun FindInList (list ^searchlist_t, name ^[8]uchar -> s32) {
	let i = s32 0
	loop while i < [list .count] {
		let lump = [[list .lumps] i]
		if W_MatchName lump name {
			return i  ; found it
		}
		i = i + 1
	}

	return -1
}

fun FindEitherInList (list ^searchlist_t, name1 ^uchar, name2 ^uchar -> s32) {
	let res = FindInList list name1

	if res < 0 {
		if name2 != NULL {
			res = FindInList list name2
		}
	}

	return res
}

fun SetupList (list ^searchlist_t, src ^ searchlist_t,
               start1 ^uchar, end1 ^uchar,
               start2 ^uchar, end2 ^uchar -> bool) {

	[list .count] = 0

	let startlump = FindEitherInList src start1 start2

	if startlump < 0 {
		return FALSE
	}

	let endlump = FindEitherInList src end1 end2

	if endlump <= startlump {
		return FALSE
	}

	startlump = startlump + 1

	[list .lumps] = [ref [src .lumps] startlump]
	[list .count] = endlump - startlump

	return TRUE
}

; Sets up the sprite/flat search lists

fun SetupLists (old_lumps s32) {
	; -- IWAD --

	[sl_iwad .lumps] = lumpdir
	[sl_iwad .count] = old_lumps

	let ok = SetupList sl_iwad_flats sl_iwad "F_START" "F_END" NULL NULL
	if not ok {
		I_Error "Flats section not found in IWAD"
	}

	ok = SetupList sl_iwad_sprites  sl_iwad "S_START" "S_END" NULL NULL
	if not ok {
		I_Error "Sprites section not found in IWAD"
	}

	; -- PWAD --

	; the PWAD was append to end of lumpinfo
	[sl_pwad .lumps] = [ref lumpdir old_lumps]
	[sl_pwad .count] = numlumps - old_lumps

	SetupList sl_pwad_flats   sl_pwad "F_START" "F_END" "FF_START" "FF_END"
	SetupList sl_pwad_sprites sl_pwad "S_START" "S_END" "SS_START" "SS_END"
}

; Initialize the replace list

fun InitSpriteList () {
	sprite_frames_num = 0
}

fun ValidSpriteLumpName (lump ^lumpinfo_t -> bool) {
	let name = [ref lump .name]

	if [name 0] == 0 {
		return FALSE
	} else if [name 1] == 0 {
		return FALSE
	} else if [name 2] == 0 {
		return FALSE
	} else if [name 3] == 0 {
		return FALSE
	} else if [name 4] == 0 {
		return FALSE
	}

	; First frame:
	let ch = [name 5]
	if ch < '0' or ch > '9' {
		return FALSE
	}

	if [name 6] == 0 {
		return TRUE
	}

	; Second frame (optional):
	ch = [name 5]
	if ch < '0' or ch > '9' {
		return FALSE
	}

	return TRUE
}

; Find a sprite frame

fun FindSpriteFrame (lump ^lumpinfo_t, frame uchar -> ^sprite_frame_t) {
	let name = [ref lump .name]

	; Search the list and try to find the frame
	let i = u32 0

	loop while i < sprite_frames_num {
		let cur = [ref sprite_frames i]

		if [cur .frame] == frame {
			if M_StrNCaseCmp [ref cur .sprite] name 4 == 0 {
				return cur
			}
		}

		i = i + 1
	}

	; Not found in list; Need to add to the list

	let result = NewSpriteFrame

	I_MemCopy [ref result .sprite] name 4

	[result .frame] = frame

	[result .angles 0] = NULL
	[result .angles 1] = NULL
	[result .angles 2] = NULL
	[result .angles 3] = NULL

	[result .angles 4] = NULL
	[result .angles 5] = NULL
	[result .angles 6] = NULL
	[result .angles 7] = NULL

	return result
}

; Check if sprite lump is needed in the new wad

fun SpriteLumpNeeded (lump ^lumpinfo_t -> bool) {
	let ok = ValidSpriteLumpName lump
	if not ok {
		; andrewj: this seems odd...
		return TRUE
	}

	; check the first frame

	let frame = FindSpriteFrame lump [lump .name 4]
	let angle = [lump .name 5] - '0'

	if SpriteFrameHasLump frame angle lump {
		return TRUE
	}

	; check second frame, if any

	if [lump .name 6] == 0 {
		return FALSE
	}

	frame = FindSpriteFrame lump [lump .name 6]
	angle = [lump .name 7] - '0'

	if SpriteFrameHasLump frame angle lump {
		return TRUE
	}

	return FALSE
}

fun SpriteFrameHasLump (frame ^sprite_frame_t, angle u8, lump ^lumpinfo_t -> bool) {
	if angle > 0 {
		; check if this lump is being used for this frame
		angle = angle - 1
		return [frame .angles angle] == lump
	}

	; must check all frames
	let i = s32 0
	loop while i < 8 {
		if [frame .angles i] == lump {
			return TRUE
		}
		i = i + 1
	}

	return FALSE
}

fun AddSpriteLump (lump ^lumpinfo_t) {
	if not ValidSpriteLumpName lump {
		return
	}

	; first angle

	let frame = FindSpriteFrame lump [lump .name 4]
	let angle = [lump .name 5] - '0'

	AddLumpToSpriteFrame frame angle lump

	; second angle, if any

	if [lump .name 6] == 0 {
		return
	}

	frame = FindSpriteFrame lump [lump .name 6]
	angle = [lump .name 7] - '0'

	AddLumpToSpriteFrame frame angle lump
}

fun AddLumpToSpriteFrame (frame ^sprite_frame_t, angle u8, lump ^lumpinfo_t) {
	if angle > 0 {
		angle = angle - 1
		[frame .angles angle] = lump
		return
	}

	; must add to all angles
	let i = s32 0
	loop while i < 8 {
		[frame .angles i] = lump
		i = i + 1
	}
}

; Generate the list.  Run at the start, before merging

fun GenerateSpriteList () {
	InitSpriteList

	; Add all sprites from the IWAD
	let i = s32 0
	loop while i < [sl_iwad_sprites .count] {
		AddSpriteLump [[sl_iwad_sprites .lumps] i]
		i = i + 1
	}

	; Add all sprites from the PWAD
	; (replaces IWAD sprites)
	i = s32 0
	loop while i < [sl_pwad_sprites .count] {
		AddSpriteLump [[sl_pwad_sprites .lumps] i]
		i = i + 1
	}
}

; Perform the merge.
;
; The merge code creates a new lumpinfo list, adding entries from the
; IWAD first followed by the PWAD.
;
; For the IWAD:
;  * Flats are added.  If a flat with the same name is in the PWAD,
;    it is ignored(deleted).  At the end of the section, all flats in the
;    PWAD are inserted.  This is consistent with the behavior of
;    deutex/deusf.
;  * Sprites are added.  The "replace list" is generated before the merge
;    from the list of sprites in the PWAD.  Any sprites in the IWAD found
;    to match the replace list are removed.  At the end of the section,
;    the sprites from the PWAD are inserted.
;
; For the PWAD:
;  * All Sprites and Flats are ignored, with the assumption they have
;    already been merged into the IWAD's sections.

fun DoMerge () {
	; can't ever have more lumps than we already have
	let size     = u32 numlumps * lumpinfo_t.size
	let newlumps = cast ^[0]^lumpinfo_t I_Alloc size

	I_MemSet newlumps 0 size

	let count = s32 0

	; --- IWAD lumps ---

	let section = u8 SECTION_NORMAL

	let i = s32 0
	loop while i < [sl_iwad .count] {
		let lump = [[sl_iwad .lumps] i]

		if section == SECTION_FLATS {
			; have we reached the end of the section?
			if W_MatchName lump "F_END" {
				; add all new flats from the PWAD to the end
				; of the section.

				let n = s32 0
				loop while n < [sl_pwad_flats .count] {
					[newlumps count] = [[sl_pwad_flats .lumps] n]
					count = count + 1
					n = n + 1
				}

				[newlumps count] = lump
				count = count + 1

				; back to normal reading
				section = SECTION_NORMAL
			} else {
				; If there is a flat in the PWAD with the same name,
				; do not add it now.  All PWAD flats are added to the
				; end of the section. Otherwise, if it is only in the
				; IWAD, add it now
				let index = FindInList sl_pwad_flats [ref lump .name]
				if index < 0 {
					[newlumps count] = lump
					count = count + 1
				}
			}

		} else if section == SECTION_SPRITES {
			; have we reached the end of the section?
			if W_MatchName lump "S_END" {
				; add all the PWAD sprites

				let n = s32 0
				loop while n < [sl_pwad_sprites .count] {
					let sprlump = [[sl_pwad_sprites .lumps] n]

					if SpriteLumpNeeded sprlump {
						[newlumps count] = sprlump
						count = count + 1
					}

					n = n + 1
				}

				; copy the ending
				[newlumps count] = lump
				count = count + 1

				; back to normal reading
				section = SECTION_NORMAL
			} else {
				; is this lump holding a sprite to be replaced in the
				; PWAD? If so, wait until the end to add it.
				if SpriteLumpNeeded lump {
					[newlumps count] = lump
					count = count + 1
				}
			}

		} else { ; SECTION_NORMAL
			if W_MatchName lump "F_START" {
				section = SECTION_FLATS
			} else if W_MatchName lump "S_START" {
				section = SECTION_SPRITES
			}

			[newlumps count] = lump
			count = count + 1
		}

		i = i + 1
	}

	; --- PWAD lumps ---

	section = SECTION_NORMAL

	i = s32 0
	loop while i < [sl_pwad .count] {
		let lump = [[sl_pwad .lumps] i]

		if section == SECTION_FLATS {
			; PWAD flats are ignored (already merged).
			; just check for end of section...

			if W_MatchName lump "FF_END" {
				section = SECTION_NORMAL
			} else if W_MatchName lump "F_END" {
				section = SECTION_NORMAL
			}

		} else if section == SECTION_SPRITES {
			; PWAD sprites are ignored (already merged)

			if W_MatchName lump "SS_END" {
				section = SECTION_NORMAL
			} else if W_MatchName lump "S_END" {
				section = SECTION_NORMAL
			}

		} else { ; SECTION_NORMAL
			if W_MatchName lump "F_START" {
				section = SECTION_FLATS
			} else if W_MatchName lump "FF_START" {
				section = SECTION_FLATS
			} else if W_MatchName lump "S_START" {
				section = SECTION_SPRITES
			} else if W_MatchName lump "SS_START" {
				section = SECTION_SPRITES
			} else {
				; don't include the headers of sections
				[newlumps count] = lump
				count = count + 1
			}
		}

		i = i + 1
	}

	; switch to the new lumpinfo, and free the old one
	W_ReplaceDirectory newlumps count
}

; Merge in a file by name

#public

fun W_MergeFile (filename ^uchar) {
	let old_lumps = numlumps

	; Load PWAD

	let wadfile = W_AddFile filename
	if wadfile == NULL {
		return
	}

	; Setup sprite/flat lists

	SetupLists old_lumps

	; Generate list of sprites to be replaced by the PWAD

	GenerateSpriteList

	; Perform the merge

	DoMerge
}

; Simulates the NWT -merge command line parameter.  What this does is load
; a PWAD, then search the IWAD sprites, removing any sprite lumps that also
; exist in the PWAD.

fun W_NWTDashMerge (filename ^uchar) {
	let old_lumps = numlumps

	; Load PWAD

	let wadfile = W_AddFile filename
	if wadfile == NULL {
		return
	}

	; Setup sprite/flat lists

	SetupLists old_lumps

	; Search through the IWAD sprites list.

	let i = s32 0
	loop {
		let count = [sl_iwad_sprites .count]
		break unless i < count

		let lump  = [[sl_iwad_sprites .lumps] i]
		let name  = [ref lump .name]
		let index = FindInList sl_pwad name

		if index >= 0 {
			; Replace this entry with an empty string.  This is what
			; nwt -merge does.

			[lump .name 0] = 0
		}

		i = i + 1
	}

	; Discard PWAD
	; The PWAD must now be added in again with -file.

	numlumps = old_lumps

	W_CloseFile wadfile
}

; Merge sprites and flats in the way NWT does with its -af and -as
; command-line options.

fun W_NWTMergeFile (filename ^uchar, sprites bool, flats bool) {
	let old_lumps = numlumps

	; Load PWAD

	let wadfile = W_AddFile filename
	if wadfile == NULL {
		return
	}

	; Setup sprite/flat lists

	SetupLists old_lumps

	; Merge in flats?

	if flats {
		W_NWTAddLumps sl_iwad_flats
	}

	; Sprites?

	if sprites {
		W_NWTAddLumps sl_iwad_sprites
	}

	; Discard the PWAD

	numlumps = old_lumps
}

#private

; Replace lumps in the given list with lumps from the PWAD

fun W_NWTAddLumps (list ^searchlist_t) {
	; Go through the IWAD list given, replacing lumps with lumps of
	; the same name from the PWAD
	let i = s32 0
	loop {
		let count = [list .count]
		break unless i < count

		let lump  = [[list .lumps] i]
		let index = FindInList sl_pwad [ref lump .name]

		if index > 0 {
			let other = [ref [sl_pwad .lumps] index]
			I_MemCopy lump other lumpinfo_t.size
		}

		i = i + 1
	}
}
