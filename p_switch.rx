;
; Copyright(C) 1993-1996 Id Software, Inc.
; Copyright(C) 2005-2014 Simon Howard
; Copyright(C)      2021 Andrew Apted
;
; This program is free software; you can redistribute it and/or
; modify it under the terms of the GNU General Public License
; as published by the Free Software Foundation; either version 2
; of the License, or (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;

#private

; max # of switch textures
const MAXSWITCHES    = 64
const MAXSWITCHES_x2 = MAXSWITCHES * 2

; 4 players, 4 buttons each at once, max.
const MAXBUTTONS = 16

; 1 second (in ticks)
const BUTTONTIME = 35

type bwhere_e = s32

const bw_free   = 0  ; a free button_t
const bw_top    = 1
const bw_middle = 2
const bw_bottom = 3

type button_t = struct {
	.line      ^line_t
	.soundorg  ^soundmobj_t
	.where      bwhere_e
	.tex        s32
	.timer      s32
}

zero-var buttonlist [MAXBUTTONS]button_t

;
; Change the texture of a wall switch to its opposite
;
type switchdef_t = struct {
	.name1    ^uchar
	.name2    ^uchar
	.episode   s32
}

rom-var switch_name_list [41]switchdef_t = {
	; Doom shareware episode 1 switches
	{ "SW1BRCOM"  "SW2BRCOM"  1 }
	{ "SW1BRN1"   "SW2BRN1"   1 }
	{ "SW1BRN2"   "SW2BRN2"   1 }
	{ "SW1BRNGN"  "SW2BRNGN"  1 }
	{ "SW1BROWN"  "SW2BROWN"  1 }
	{ "SW1COMM"   "SW2COMM"   1 }
	{ "SW1COMP"   "SW2COMP"   1 }
	{ "SW1DIRT"   "SW2DIRT"   1 }
	{ "SW1EXIT"   "SW2EXIT"   1 }
	{ "SW1GRAY"   "SW2GRAY"   1 }
	{ "SW1GRAY1"  "SW2GRAY1"  1 }
	{ "SW1METAL"  "SW2METAL"  1 }
	{ "SW1PIPE"   "SW2PIPE"   1 }
	{ "SW1SLAD"   "SW2SLAD"   1 }
	{ "SW1STARG"  "SW2STARG"  1 }
	{ "SW1STON1"  "SW2STON1"  1 }
	{ "SW1STON2"  "SW2STON2"  1 }
	{ "SW1STONE"  "SW2STONE"  1 }
	{ "SW1STRTN"  "SW2STRTN"  1 }

	; Doom registered episodes 2 and 3 switches
	{ "SW1BLUE"   "SW2BLUE"   2 }
	{ "SW1CMT"    "SW2CMT"    2 }
	{ "SW1GARG"   "SW2GARG"   2 }
	{ "SW1GSTON"  "SW2GSTON"  2 }
	{ "SW1HOT"    "SW2HOT"    2 }
	{ "SW1LION"   "SW2LION"   2 }
	{ "SW1SATYR"  "SW2SATYR"  2 }
	{ "SW1SKIN"   "SW2SKIN"   2 }
	{ "SW1VINE"   "SW2VINE"   2 }
	{ "SW1WOOD"   "SW2WOOD"   2 }

	; Doom II switches
	{ "SW1PANEL"  "SW2PANEL"  3 }
	{ "SW1ROCK"   "SW2ROCK"   3 }
	{ "SW1MET2"   "SW2MET2"   3 }
	{ "SW1WDMET"  "SW2WDMET"  3 }
	{ "SW1BRIK"   "SW2BRIK"   3 }
	{ "SW1MOD1"   "SW2MOD1"   3 }
	{ "SW1ZIM"    "SW2ZIM"    3 }
	{ "SW1STON6"  "SW2STON6"  3 }
	{ "SW1TEK"    "SW2TEK"    3 }
	{ "SW1MARB"   "SW2MARB"   3 }
	{ "SW1SKULL"  "SW2SKULL"  3 }

	; end marker
	{ NULL NULL -1 }
}

; texture numbers, name1 + name2 pairs
zero-var switchlist   [MAXSWITCHES_x2]s32
zero-var numswitchtex s32

;
; P_InitSwitchList
; Only called at game initialization.
;
fun P_InitSwitchList () {
	; Note that this is called "episode" here but it's actually something
	; quite different: as we progress from Shareware->Registered->Doom II
	; we support more switch textures.
	let max_episode = s32 1

	if gamemode == registered {
		max_episode = 2
	} else if gamemode == retail {
		max_episode = 2
	} else if gamemode == commercial {
		max_episode = 3
	}

	let in  = s32 0
	let out = s32 0

	loop {
		let def = [ref switch_name_list in]

		break if null? [def .name1]

		if [def .episode] <= max_episode {
			[switchlist out] = R_TextureNumForName [def .name1]
			out = out + 1

			[switchlist out] = R_TextureNumForName [def .name2]
			out = out + 1
		}

		in = in + 1
	}

	numswitchtex = out
}

fun P_InitButtons () {
	I_MemSet buttonlist 0 (MAXBUTTONS * u32 button_t.size)
}

;
; Start a button counting down till it turns off.
;
fun P_StartButton (ld ^line_t, where bwhere_e, texture s32, time s32) {
	; See if button is already pressed
	let i = s32 0
	loop while i < MAXBUTTONS {
		let but = [ref buttonlist i]

		if [but .where] != bw_free and [but .line] == ld {
			return
		}

		i = i + 1
	}

	; find a free slot
	i = s32 0
	loop while i < MAXBUTTONS {
		let but = [ref buttonlist i]

		if [but .where] == bw_free {
			[but .line]     = ld
			[but .soundorg] = [ref [ld .front] .soundorg]
			[but .where]    = where
			[but .tex]      = texture
			[but .timer]    = time
			return
		}

		i = i + 1
	}

	; andrewj: ignore this instead of bombing out
	; I_Error("P_StartButton: no button slots left!")
}

;
; Function that changes wall texture.
; Tell it if switch is ok to use again (1=yes, it's a button).
;
fun P_ChangeSwitchTexture (ld ^line_t, use_again bool) {
	if not use_again {
		[ld .special] = 0
	}

	let side = [ref sides [ld .sidenum 0]]

	; andrewj: vanilla uses first entry in buttonlist[] for the sound origin.
	; that is gonna do the wrong thing occasionally.  I think the following
	; is reasonable, and sound playing does not affect demo sync.
	let origin = [ref [ld .front] .soundorg]

	let sound = sfxenum_t sfx_swtchn

	; EXIT SWITCH?
	if [ld .special] == 11 {
		sound = sfx_swtchx
	}

	let i = s32 0
	loop while i < numswitchtex {
		let k = i ~ 1
		let cur_tex = [switchlist i]

		if cur_tex == [side .top] {
			[side .top] = [switchlist k]

			if use_again {
				P_StartButton ld bw_top cur_tex BUTTONTIME
			}

			S_StartSound origin sound
			return

		} else if cur_tex == [side .mid] {
			[side .mid] = [switchlist k]

			if use_again {
				P_StartButton ld bw_middle cur_tex BUTTONTIME
			}

			S_StartSound origin sound
			return

		} else if cur_tex == [side .bottom] {
			[side .bottom] = [switchlist k]

			if use_again {
				P_StartButton ld bw_bottom cur_tex BUTTONTIME
			}

			S_StartSound origin sound
			return
		}

		i = i + 1
	}
}

fun P_UpdateButtons () {
	let i = s32 0
	loop while i < MAXBUTTONS {
		let but = [ref buttonlist i]

		if [but .where] != bw_free {
			[but .timer] = [but .timer] - 1

			if [but .timer] == 0 {
				P_FinishButton but TRUE
			}
		}

		i = i + 1
	}
}

fun P_FinishAllButtons () {
	let i = s32 0
	loop while i < MAXBUTTONS {
		let but = [ref buttonlist i]

		if [but .where] != bw_free {
			P_FinishButton but FALSE
		}

		i = i + 1
	}
}

fun P_FinishButton (but ^button_t, do_sound bool) {
	let where = [but .where]
	let ld    = [but .line]
	let side  = [ref sides [ld .sidenum 0]]

	if where == bw_top {
		[side .top] = [but .tex]

	} else if where == bw_middle {
		[side .mid] = [but .tex]

	} else if where == bw_bottom {
		[side .bottom] = [but .tex]
	}

	; mark slot as free
	[but .where] = bw_free

	if do_sound {
		S_StartSound [but .soundorg] sfx_swtchn
	}
}

;
; P_UseSpecialLine
;
; Called when a thing uses a special line.
; Only the front sides of lines are usable.
;
fun P_UseSpecialLine (ld ^line_t, side s32, mo ^mobj_t -> bool) {
	; Err...
	; Use the back sides of VERY SPECIAL lines...

	if side != 0 {
		; Ummm... not really
		return FALSE
	}

	let spec = [ld .special]

	; Switches that monsters can activate.
	if null? [mo .player] {
		; never open secret doors
		if ([ld .flags] & ML_SECRET) != 0 {
			return FALSE
		}

		if matches? spec 1 32 33 34 {
			; MANUAL DOORS are ok
		} else {
			return FALSE
		}
	}

	; do something!

	; MANUAL DOORS
	jump manual if spec == 1   ; Vertical Door
	jump manual if spec == 26  ; Blue Door/Locked
	jump manual if spec == 27  ; Yellow Door /Locked
	jump manual if spec == 28  ; Red Door /Locked

	jump manual if spec == 31  ; Manual door open
	jump manual if spec == 32  ; Blue locked door open
	jump manual if spec == 33  ; Red locked door open
	jump manual if spec == 34  ; Yellow locked door open

	jump manual if spec == 117 ; Blazing door raise
	jump manual if spec == 118 ; Blazing door open

	; REMOTE LOCKED DOORS
	jump lock_once if spec == 133  ; BlzOpenDoor BLUE
	jump lock_once if spec == 135  ; BlzOpenDoor RED
	jump lock_once if spec == 137  ; BlzOpenDoor YELLOW

	jump lock_again if spec == 99  ; BlzOpenDoor BLUE
	jump lock_again if spec == 134 ; BlzOpenDoor RED
	jump lock_again if spec == 136 ; BlzOpenDoor YELLOW

	; SWITCHES
	let did = TRUE

	if spec == 7 {  ; Build Stairs
		did = EV_BuildStairs ld stair_build8
		jump once_only

	} else if spec == 9 {  ; Change Donut
		did = EV_DoDonut ld
		jump once_only

	} else if spec == 11 {  ; Exit level
		G_ExitLevel
		jump once_only

	} else if spec == 14 {  ; Raise Floor 32 and change texture
		did = EV_DoPlat ld pl_raiseAndChange 32
		jump once_only

	} else if spec == 15 {  ; Raise Floor 24 and change texture
		did = EV_DoPlat ld pl_raiseAndChange 24
		jump once_only

	} else if spec == 18 {  ; Raise Floor to next highest floor
		did = EV_DoFloor ld fl_raiseToNearest
		jump once_only

	} else if spec == 20 {  ; Raise Plat next highest floor and change texture
		did = EV_DoPlat ld pl_raiseToNearest 0
		jump once_only

	} else if spec == 21 {  ; PlatDownWaitUpStay
		did = EV_DoPlat ld pl_downWaitUpStay 0
		jump once_only

	} else if spec == 23 {  ; Lower Floor to Lowest
		did = EV_DoFloor ld fl_lowerToLowest
		jump once_only

	} else if spec == 29 {  ; Raise Door
		did = EV_DoDoor ld dr_normal
		jump once_only

	} else if spec == 41 {  ; Lower Ceiling to Floor
		did = EV_DoCeiling ld ce_lowerToFloor
		jump once_only

	} else if spec == 71 {  ; Turbo Lower Floor
		did = EV_DoFloor ld fl_lowerTurbo
		jump once_only

	} else if spec == 49 {  ; Ceiling Crush And Raise
		did = EV_DoCeiling ld ce_crushAndRaise
		jump once_only

	} else if spec == 50 {  ; Close Door
		did = EV_DoDoor ld dr_close
		jump once_only

	} else if spec == 51 {  ; Secret EXIT
		G_SecretExitLevel
		jump once_only

	} else if spec == 55 {  ; Raise Floor Crush
		did = EV_DoFloor ld fl_raiseFloorCrush
		jump once_only

	} else if spec == 101 {  ; Raise Floor
		did = EV_DoFloor ld fl_raiseFloor
		jump once_only

	} else if spec == 102 {  ; Lower Floor to Surrounding floor height
		did = EV_DoFloor ld fl_lowerFloor
		jump once_only

	} else if spec == 103 {  ; Open Door
		did = EV_DoDoor ld dr_open
		jump once_only

	} else if spec == 111 {  ; Blazing Door Raise (faster than TURBO!)
		did = EV_DoDoor  ld dr_blazeRaise
		jump once_only

	} else if spec == 112 {  ; Blazing Door Open (faster than TURBO!)
		did = EV_DoDoor  ld dr_blazeOpen
		jump once_only

	} else if spec == 113 {  ; Blazing Door Close (faster than TURBO!)
		did = EV_DoDoor  ld dr_blazeClose
		jump once_only

	} else if spec == 122 {  ; Blazing PlatDownWaitUpStay
		did = EV_DoPlat ld pl_downWaitBlaze 0
		jump once_only

	} else if spec == 127 {  ; Build Stairs Turbo 16
		did = EV_BuildStairs ld stair_turbo16
		jump once_only

	} else if spec == 131 {  ; Raise Floor Turbo
		did = EV_DoFloor ld fl_raiseNearestTurbo
		jump once_only

	} else if spec == 140 {  ; Raise Floor 512
		did = EV_DoFloor ld fl_raiseFloor512
		jump once_only

	} else if spec == 42 {  ; Close Door
		did = EV_DoDoor ld dr_close
		jump use_again

	} else if spec == 43 {  ; Lower Ceiling to Floor
		did = EV_DoCeiling ld ce_lowerToFloor
		jump use_again

	} else if spec == 45 {  ; Lower Floor to Surrounding floor height
		did = EV_DoFloor ld fl_lowerFloor
		jump use_again

	} else if spec == 60 {  ; Lower Floor to Lowest
		did = EV_DoFloor ld fl_lowerToLowest
		jump use_again

	} else if spec == 61 {  ; Open Door
		did = EV_DoDoor ld dr_open
		jump use_again

	} else if spec == 62 {  ; PlatDownWaitUpStay
		did = EV_DoPlat ld pl_downWaitUpStay 0
		jump use_again

	} else if spec == 63 {  ; Raise Door
		did = EV_DoDoor ld dr_normal
		jump use_again

	} else if spec == 64 {  ; Raise Floor to ceiling
		did = EV_DoFloor ld fl_raiseFloor
		jump use_again

	} else if spec == 66 {  ; Raise Floor 24 and change texture
		did = EV_DoPlat ld pl_raiseAndChange 24
		jump use_again

	} else if spec == 67 {  ; Raise Floor 32 and change texture
		did = EV_DoPlat ld pl_raiseAndChange 32
		jump use_again

	} else if spec == 65 {  ; Raise Floor Crush
		did = EV_DoFloor ld fl_raiseFloorCrush
		jump use_again

	} else if spec == 68 {  ; Raise Plat to next highest floor and change texture
		did = EV_DoPlat ld pl_raiseToNearest 0
		jump use_again

	} else if spec == 69 {  ; Raise Floor to next highest floor
		did = EV_DoFloor ld fl_raiseToNearest
		jump use_again

	} else if spec == 70 {  ; Turbo Lower Floor
		did = EV_DoFloor ld fl_lowerTurbo
		jump use_again

	} else if spec == 114 {  ; Blazing Door Raise (faster than TURBO!)
		did = EV_DoDoor  ld dr_blazeRaise
		jump use_again

	} else if spec == 115 {  ; Blazing Door Open (faster than TURBO!)
		did = EV_DoDoor  ld dr_blazeOpen
		jump use_again

	} else if spec == 116 {  ; Blazing Door Close (faster than TURBO!)
		did = EV_DoDoor  ld dr_blazeClose
		jump use_again

	} else if spec == 123 {  ; Blazing PlatDownWaitUpStay
		did = EV_DoPlat ld pl_downWaitBlaze 0
		jump use_again

	} else if spec == 132 {  ; Raise Floor Turbo
		did = EV_DoFloor ld fl_raiseNearestTurbo
		jump use_again

	} else if spec == 138 {  ; Light Turn On
		EV_LightTurnOn ld 255
		jump use_again

	} else if spec == 139 {  ; Light Turn Off
		EV_LightTurnOn ld 35
		jump use_again
	}

	; unknown type, ignore it
	return TRUE

	do manual {
		EV_ManualDoor ld mo
		return TRUE
	}

	do lock_once {
		did = EV_DoLockedDoor ld dr_blazeOpen mo
	}

	do once_only {
		if did {
			P_ChangeSwitchTexture ld FALSE
		}
		return TRUE
	}

	do lock_again {
		did = EV_DoLockedDoor ld dr_blazeOpen mo
	}

	do use_again {
		if did {
			P_ChangeSwitchTexture ld TRUE
		}
		return TRUE
	}
}

;
; P_ShootSpecialLine - IMPACT SPECIALS
; Called when a thing shoots a special line.
;
fun P_ShootSpecialLine (ld ^line_t, mo ^mobj_t) {
	let spec = [ld .special]

	; Impacts that monsters can activate.
	if null? [mo .player] {
		if spec == 46 {
			; ok
		} else {
			return
		}
	}

	if spec == 24 {
		; RAISE FLOOR
		EV_DoFloor ld fl_raiseFloor
		P_ChangeSwitchTexture ld FALSE

	} else if spec == 46 {
		; OPEN DOOR
		EV_DoDoor ld dr_open
		P_ChangeSwitchTexture ld TRUE

	} else if spec == 47 {
		; RAISE FLOOR NEAR AND CHANGE
		EV_DoPlat ld pl_raiseToNearest 0
		P_ChangeSwitchTexture ld FALSE
	}
}
